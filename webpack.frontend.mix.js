const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'resources/frontend/css/font-awesome.min.css',
    'resources/frontend/css/material-design-iconic-font.min.css',
    'resources/frontend/css/bootstrap.min.css',
    'resources/frontend/css/plugins.css',
    'resources/frontend/css/style.css',
    'resources/frontend/css/color.css',
    'resources/frontend/css/responsive.css',
    ///////////////////////////////////////////////////
    'resources/lib/datatables/css/jquery.dataTables.css',
    'resources/lib/jt.timepicker/css/jquery.timepicker.css',
    'resources/lib/bootstrap-tagsinput/css/bootstrap-tagsinput.css',
    'resources/lib/select2/css/select2.min.css',
    'resources/lib/trumbowyg/dist/ui/trumbowyg.css',
    ///////////////////////////////////////////////////
    'resources/frontend/css/custom.css',
], 'public/frontend/css/app.css');

mix.copy('resources/frontend/css/rtl.css', 'public/frontend/css/rtl.css');

mix.scripts([
    'resources/frontend/js/vendor/modernizr-2.8.3.min.js',
    'resources/frontend/js/vendor/jquery.js',
    'resources/frontend/js/popper.min.js',
    'resources/frontend/js/bootstrap.min.js',
    'resources/frontend/js/plugins.js',
    'resources/frontend/lib/notify/js/notify.js',
    ///////////////////////////////////////////////////
    'resources/lib/jquery-ui/js/jquery-ui.js',
    'resources/lib/datatables/js/jquery.dataTables.js',
    'resources/lib/bootstrap-tagsinput/js/bootstrap-tagsinput.js',
    'resources/lib/select2/js/select2.min.js',
    'resources/lib/trumbowyg/dist/trumbowyg.js',
    ///////////////////////////////////////////////////
    'resources/frontend/js/main.js',
], 'public/frontend/js/app.js');
mix.js('resources/js/app.js', 'public/backend/js');
mix.copyDirectory('resources/frontend/fonts', 'public/frontend/fonts');
mix.copyDirectory('resources/frontend/img', 'public/frontend/img');
mix.copyDirectory('resources/frontend/img', 'public/frontend/img');
if (mix.inProduction()) {
    mix.version();
}

//mix.browserSync('http://localhost:8000');


