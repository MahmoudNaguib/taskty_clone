<?php

////////////// Default Data
function insertDefaultTokens() {
    $rows = [
        [
            'user_id' => 4,
            'token' => 'sf13bf6d4241be097a975f718adbb179c81e728d9d4c2f636f067f89cc14862c5e33b9abc68925efdc88fe95bf7ac767',
            'expiry_at' => date('Y-m-d', strtotime("+" . env('TOKEN_EXPIRATION', 100) . " days")),
            'device' => 'anoynomous',
            'push_token' => 'anoynomous'
        ]
    ];
    \DB::table('tokens')->insert($rows);
}

function insertDefaultRoles() {
    $rows = [
        [
            'id' => 1,
            'title' => 'Super Administrator',
            'permissions' => json_encode(permissions()),
            'created_by' => 2,
            'is_default' => 1,
        ],
        [
            'id' => 2,
            'title' => 'Moderator',
            'permissions' => json_encode(permissions()),
            'created_by' => 2,
            'is_default' => 0,
        ]
    ];
    \DB::table('roles')->insert($rows);
}

function insertDefaultUsers() {
    $users = [
        [
            'id' => 1,
            'role_id' => 1,
            'type' => 'admin',
            'language' => 'en',
            'gender'=>'m',
            'date_of_birth'=>'1982-11-06',
            'country_id'=>env('COUNTRY_ID'),
            'city'=>env('CITY'),
            'street_name'=>'',
            'house_number'=>'',
            'postal_code'=>'',
            'confirmed' => 1,
            'is_active' => 1,
            'role_id' => 1,
            'name' => 'Super Admin',
            'email' => "super@demo.com",
            'mobile' => '0122'.rand(1000000,9999999),
            'password' => bcrypt('demo@12345'),
            'image' => generateImage('Super Admin'),
            'created_by' => 2,
            'is_default' => 1,
            'is_tasker' => 0,
            'bio' => 'Bio details',
        ],
        [
            'id' => 2,
            'role_id' => 1,
            'type' => 'admin',
            'gender'=>'m',
            'language' => 'en',
            'date_of_birth'=>'1982-11-06',
            'country_id'=>env('COUNTRY_ID'),
            'city'=>env('CITY'),
            'street_name'=>'',
            'house_number'=>'',
            'postal_code'=>'',
            'confirmed' => 1,
            'is_active' => 1,
            'name' => 'Admin',
            'email' => "admin@demo.com",
            'mobile' => '0122'.rand(1000000,9999999),
            'password' => bcrypt('demo@12345'),
            'image' => generateImage('Admin'),
            'created_by' => 2,
            'is_default' => 1,
            'is_tasker' => 0,
            'bio' => 'Bio details',
        ],
        [
            'id' => 3,
            'type' => 'user',
            'role_id' => NULL,
            'language' => 'en',
            'gender'=>'m',
            'date_of_birth'=>'1982-11-06',
            'country_id'=>env('COUNTRY_ID'),
            'city'=>env('CITY'),
            'street_name'=>'',
            'house_number'=>'',
            'postal_code'=>'',
            'confirmed' => 1,
            'is_active' => 1,
            'name' => 'User 1',
            'email' => 'user1@demo.com',
            'mobile' => '0122'.rand(1000000,9999999),
            'password' => bcrypt('demo@12345'),
            'image' => generateImage('User1'),
            'created_by' => 2,
            'is_default' => 0,
            'is_tasker' => 1,
            'bio' => 'Bio details',
        ],
        [
            'id' => 4,
            'type' => 'user',
            'role_id' => NULL,
            'language' => 'en',
            'gender'=>'m',
            'date_of_birth'=>'1982-11-06',
            'country_id'=>env('COUNTRY_ID'),
            'city'=>env('CITY'),
            'street_name'=>'',
            'house_number'=>'',
            'postal_code'=>'',
            'confirmed' => 1,
            'is_active' => 1,
            'name' => 'User2',
            'email' => 'user2@demo.com',
            'mobile' => '0122'.rand(1000000,9999999),
            'password' => bcrypt('demo@12345'),
            'image' => generateImage('User2'),
            'created_by' => 2,
            'is_default' => 0,
            'is_tasker' => 1,
            'bio' => 'Bio details for user 1',
        ],
        [
            'id' => 5,
            'type' => 'user',
            'role_id' => NULL,
            'language' => 'en',
            'gender'=>'m',
            'date_of_birth'=>'1982-11-06',
            'country_id'=>env('COUNTRY_ID'),
            'city'=>env('CITY'),
            'street_name'=>'',
            'house_number'=>'',
            'postal_code'=>'',
            'confirmed' => 1,
            'is_active' => 1,
            'name' => 'User3',
            'email' => 'user3@demo.com',
            'mobile' => '0122'.rand(1000000,9999999),
            'password' => bcrypt('demo@12345'),
            'image' => generateImage('User3'),
            'created_by' => 2,
            'is_default' => 0,
            'is_tasker' => 1,
            'bio' => 'Bio details for User3',
        ],
        [
            'id' => 6,
            'type' => 'user',
            'role_id' => NULL,
            'language' => 'en',
            'gender'=>'m',
            'date_of_birth'=>'1982-11-06',
            'country_id'=>env('COUNTRY_ID'),
            'city'=>env('CITY'),
            'street_name'=>'',
            'house_number'=>'',
            'postal_code'=>'',
            'confirmed' => 1,
            'is_active' => 1,
            'name' => 'User4',
            'email' => 'user4@demo.com',
            'mobile' => '0122'.rand(1000000,9999999),
            'password' => bcrypt('demo@12345'),
            'image' => generateImage('User4'),
            'created_by' => 2,
            'is_default' => 0,
            'is_tasker' => 1,
            'bio' => 'Bio details for User2',
        ]
    ];
    \DB::table('users')->insert($users);
}

function insertDefaultConfigs() {
    \Cache::forget('configs');
    @copy(resource_path() . '/frontend/img/logo.png', public_path() . '/uploads/small/logo.png');
    @copy(resource_path() . '/frontend/img/logo.png', public_path() . '/uploads/large/logo.png');
    @copy(resource_path() . '/frontend/img/home_banner.jpg', public_path() . '/uploads/small/home_banner.jpg');
    @copy(resource_path() . '/frontend/img/home_banner.jpg', public_path() . '/uploads/large/home_banner.jpg');
    @copy(resource_path() . '/frontend/img/why_banner.jpg', public_path() . '/uploads/small/why_banner.jpg');
    @copy(resource_path() . '/frontend/img/why_banner.jpg', public_path() . '/uploads/large/why_banner.jpg');
    @copy(resource_path() . '/frontend/img/facts_banner.jpg', public_path() . '/uploads/small/facts_banner.jpg');
    @copy(resource_path() . '/frontend/img/facts_banner.jpg', public_path() . '/uploads/large/facts_banner.jpg');
    @copy(resource_path() . '/frontend/img/about_banner.jpg', public_path() . '/uploads/small/about_banner.jpg');
    @copy(resource_path() . '/frontend/img/about_banner.jpg', public_path() . '/uploads/large/about_banner.jpg');


    $rows = [];
    //////////// Basic Information
    $txt = env('APP_NAME');
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'General',
            'field' => 'application_name',
            'label' => 'Application Name',
            'value' => $txt,
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'General',
        'field' => 'logo',
        'label' => 'Logo',
        'value' => 'logo.png',
        'lang' => NULL,
        'created_by' => 2,
    ];
    /////////////// Home Page
    //////////// Home Page
    $txt = 'Find the people with the skills you need';
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'Home Page',
            'field' => 'home_banner_text',
            'label' => 'Home banner text',
            'value' => $txt,
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    $txt = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text amr songr balga ami toami valo lasi ciri din akr dali";
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Home Page',
            'field' => 'home_why_text',
            'label' => 'Home why text',
            'value' => $txt,
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    $txt = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text amr songr balga ami toami valo lasi ciri din akr dali";
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Home Page',
            'field' => 'home_why_text',
            'label' => 'Home why text',
            'value' => $txt,
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }

    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Home Page',
            'field' => 'home_meta_description',
            'label' => 'Meta description',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => 'tags',
            'type' => 'Home Page',
            'field' => 'home_meta_keywords',
            'label' => 'Meta keywords',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }

    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'Home Page',
        'field' => 'home_banner',
        'label' => 'Home banner',
        'value' => 'home_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'Home Page',
        'field' => 'why_banner',
        'label' => 'Why banner',
        'value' => 'why_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'Home Page',
        'field' => 'facts_banner',
        'label' => 'Facts banner',
        'value' => 'facts_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    ///////////////////////// About us
    $txt = "Lorizzle shizzlin dizzle stuff bow wow wow amizzle, stuff adipiscing elit. Nullizzle sapizzle velit, i saw beyonces tizzles and my pizzle went crizzle volutpizzle, its fo rizzle quis, gravida vizzle, arcu. Pellentesque check it out shizzlin dizzle. Fo shizzle mah nizzle fo rizzle, mah home g-dizzle erizzle. Owned shizzle my nizzle crocodizzle dolor dapibizzle turpizzle tempizzle mah nizzle. Mauris we gonna chung nibh et turpizzle. Vestibulum in tortor. Pellentesque eleifend rhoncus i'm in the shizzle. In hac habitasse platea i saw beyonces tizzles and my pizzle went crizzle. Dope dapibizzle. Curabitizzle mah nizzle gangster, funky fresh eu, mattizzle fo shizzle, eleifend vitae, nunc. Fo shizzle suscipit. Integizzle rizzle velit sed fo shizzle.";
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => 'editor',
            'type' => 'About Us',
            'field' => 'about_text',
            'label' => 'About us',
            'value' => $txt,
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'About Us',
        'field' => 'about_banner',
        'label' => 'Banner',
        'value' => 'about_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'About Us',
            'field' => 'about_meta_description',
            'label' => 'Meta description',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => 'tags',
            'type' => 'About Us',
            'field' => 'about_meta_keywords',
            'label' => 'Meta keywords',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }

    ///////////////////////// Contact us
    $txt = "Lorizzle shizzlin dizzle stuff bow wow wow amizzle, stuff adipiscing elit. Nullizzle sapizzle velit, i saw beyonces tizzles and my pizzle went crizzle volutpizzle, its fo rizzle quis, gravida vizzle, arcu. Pellentesque check it out shizzlin dizzle. Fo shizzle mah nizzle fo rizzle, mah home g-dizzle erizzle. Owned shizzle my nizzle crocodizzle dolor dapibizzle turpizzle tempizzle mah nizzle. Mauris we gonna chung nibh et turpizzle. Vestibulum in tortor. Pellentesque eleifend rhoncus i'm in the shizzle. In hac habitasse platea i saw beyonces tizzles and my pizzle went crizzle. Dope dapibizzle. Curabitizzle mah nizzle gangster, funky fresh eu, mattizzle fo shizzle, eleifend vitae, nunc. Fo shizzle suscipit. Integizzle rizzle velit sed fo shizzle.";
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => 'editor',
            'type' => 'Contact us',
            'field' => 'contact_text',
            'label' => 'Contact us text',
            'value' => $txt,
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'Contact us',
        'field' => 'contact_banner',
        'label' => 'Banner',
        'value' => 'about_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Contact us',
            'field' => 'contact_meta_description',
            'label' => 'Meta description',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => 'tags',
            'type' => 'Contact us',
            'field' => 'contact_meta_keywords',
            'label' => 'Meta keywords',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    ///////////////////////// Terms
    $txt = "Lorizzle shizzlin dizzle stuff bow wow wow amizzle, stuff adipiscing elit. Nullizzle sapizzle velit, i saw beyonces tizzles and my pizzle went crizzle volutpizzle, its fo rizzle quis, gravida vizzle, arcu. Pellentesque check it out shizzlin dizzle. Fo shizzle mah nizzle fo rizzle, mah home g-dizzle erizzle. Owned shizzle my nizzle crocodizzle dolor dapibizzle turpizzle tempizzle mah nizzle. Mauris we gonna chung nibh et turpizzle. Vestibulum in tortor. Pellentesque eleifend rhoncus i'm in the shizzle. In hac habitasse platea i saw beyonces tizzles and my pizzle went crizzle. Dope dapibizzle. Curabitizzle mah nizzle gangster, funky fresh eu, mattizzle fo shizzle, eleifend vitae, nunc. Fo shizzle suscipit. Integizzle rizzle velit sed fo shizzle.";
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => 'editor',
            'type' => 'Terms And Conditions',
            'field' => 'terms_text',
            'label' => 'Terms And Conditions',
            'value' => $txt,
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'Terms And Conditions',
        'field' => 'terms_banner',
        'label' => 'Banner',
        'value' => 'about_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Terms And Conditions',
            'field' => 'terms_meta_description',
            'label' => 'Meta description',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => 'tags',
            'type' => 'Terms And Conditions',
            'field' => 'terms_meta_keywords',
            'label' => 'Meta keywords',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    ///////////////////////// Privay Policy
    $txt = "Lorizzle shizzlin dizzle stuff bow wow wow amizzle, stuff adipiscing elit. Nullizzle sapizzle velit, i saw beyonces tizzles and my pizzle went crizzle volutpizzle, its fo rizzle quis, gravida vizzle, arcu. Pellentesque check it out shizzlin dizzle. Fo shizzle mah nizzle fo rizzle, mah home g-dizzle erizzle. Owned shizzle my nizzle crocodizzle dolor dapibizzle turpizzle tempizzle mah nizzle. Mauris we gonna chung nibh et turpizzle. Vestibulum in tortor. Pellentesque eleifend rhoncus i'm in the shizzle. In hac habitasse platea i saw beyonces tizzles and my pizzle went crizzle. Dope dapibizzle. Curabitizzle mah nizzle gangster, funky fresh eu, mattizzle fo shizzle, eleifend vitae, nunc. Fo shizzle suscipit. Integizzle rizzle velit sed fo shizzle.";
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => 'editor',
            'type' => 'Privacy policy',
            'field' => 'privacy_text',
            'label' => 'Privacy policy',
            'value' => $txt,
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'Privacy policy',
        'field' => 'privacy_banner',
        'label' => 'Banner',
        'value' => 'about_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Privacy policy',
            'field' => 'privacy_meta_description',
            'label' => 'Meta description',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => 'tags',
            'type' => 'Privacy policy',
            'field' => 'privacy_meta_keywords',
            'label' => 'Meta keywords',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }

    ///////////////// Contact information
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'email',
        'label' => 'Email',
        'value' => env('CONTACT_EMAIL'),
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'phone',
        'label' => 'Phone',
        'value' => '12345678',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'mobile',
        'label' => 'Mobile',
        'value' => '12345678',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $txt = "Address will be added here";
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Contact Information',
            'field' => 'address',
            'label' => 'Address',
            'value' => $txt,
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }

    ///////////////// Social Links
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Social Links',
        'field' => 'facebook_link',
        'label' => 'Facebook link',
        'value' => 'http://www.facebook.com/sunnyworld',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Social Links',
        'field' => 'instagram_link',
        'label' => 'Instagram link',
        'value' => 'http://www.instagram.com',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Social Links',
        'field' => 'twitter_link',
        'label' => 'Twitter link',
        'value' => 'http://www.twitter.com',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Social Links',
        'field' => 'youtube_link',
        'label' => 'Youtube link',
        'value' => 'http://www.youtube.com',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'map_url',
        'label' => 'Map url',
        'value' => 'https://goo.gl/maps/cJXRmpoCUS66PutW7',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'contact_submit_email',
        'label' => 'Contact submit email',
        'value' => env('CONTACT_EMAIL'),
        'lang' => NULL,
        'created_by' => 2,
    ];


    \DB::table('configs')->insert($rows);
}

function insertDummyChildSections() {
    $parentsIds = App\Models\Section::where('parent_id', NULL)->pluck('id')->toArray();
    $rows = [];
    for ($i = 0; $i <= 20; $i++) {
        $rows[] = [
            'parent_id' => $parentsIds[array_rand($parentsIds)],
            'title' => 'Child section ' . rand(1, 1000),
            'created_by' => 2,
            'is_active' => 1
        ];
    }
    if ($rows) {
        foreach ($rows as &$row) {
            foreach (langs() as $lang) {
                $title[$lang] = $row['title'];
            }
            $row['title'] = json_encode($title);
        }
    }
    \DB::table('sections')->insert($rows);
}
