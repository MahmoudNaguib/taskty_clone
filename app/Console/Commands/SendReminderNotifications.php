<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Reminder;

class SendReminderNotifications extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminders:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification for reminder user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $nextHour = str_replace(date('h:i A'), sprintf('%02d', date('h') + 1) . ':00 ' . date('A'), date('h:i A'));
        $rows = \App\Models\Reminder::where('date', date('Y-m-d'))->where('time', '>', date('h:i A'))->where('time', '<', $nextHour)->latest()->get();
        if ($rows) {
            foreach ($rows as $row) {
                \App\Jobs\SendReminderMail::dispatch($row);
            }
        }
    }

}
