<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Search extends \App\Models\BaseModel {

    protected $table = "search";
    protected $fillable = ['key', 'value', 'total'];

}
