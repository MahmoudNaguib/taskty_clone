<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable;

    ///////////////////////////// has translation
    protected $table = "tasks";
    protected $appends = ['link', 'title_limited', 'content_limited'];
    protected $guarded = [
        'deleted_at',
        'logged_user',
        'image',
        'attachment',
    ];
    protected $hidden = [
        'deleted_at',
    ];
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,240x180', 'large' => 'resize,720x540'],
            'path' => 'uploads'
        ],
        'attachment' => [
            'path' => 'uploads'
        ],
    ];
    public $rules = [
        'created_by' => 'required',
        'section_id' => 'required',
        'title' => 'required',
        'content' => 'required',
        'where' => 'required',
        'country_id' => "required",
        'location' => 'required',
        'longitude' => 'required',
        'latitude' => 'required',
        'date' => 'required|date|after:today',
        'time' => 'required',
        'budget' => 'required',
        'attachment' => 'nullable|max:8000'
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
        ];
        return $array;
    }

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage($row->title, ['small' => '240x180', 'large' => '720x540']);
                $data['image'] = $image;
                \DB::table('tasks')->where('id', $row->id)->update($data);
            }
        });
    }

    public function getSectionsWithParents() {
        return \App\Models\Section::with(['main'])->active()->where('parent_id', '!=', NULL)->get(['id', 'parent_id', 'title'])->groupBy('main.title')
                        ->map(function ($item) {
                            return $item->pluck('title', 'id');
                        });
    }

    public function getCountries() {
        return \App\Models\Country::pluck('title', 'id');
    }

    public function getCreators() {
        return \App\Models\User::where('is_tasker', '=', 1)->pluck('name', 'id');
    }

    public function getWhereValues() {
        $rows = [
            'in_person' => trans('tasks.In person'),
            'remote' => trans('tasks.Remote')
        ];
        return $rows;
    }

    public function getWhereValueAttribute() {
        return $this->getWhereValues()[$this->where];
    }

    public function getTimeValues() {
        $rows = [
            'before_10am' => trans('tasks.Before 10am'),
            '10am_2pm' => trans('tasks.10am to 2pm'),
            'after_2pm_6pm' => trans('tasks.After 2pm to 6pm'),
            'after_6pm' => trans('tasks.After 6pm'),
        ];
        return $rows;
    }

    public function getTimeValueAttribute() {
        return $this->getTimeValues()[$this->time];
    }

    public function getStatusValues() {
        $rows = [
            'draft' => trans('tasks.Draft'),
            'open' => trans('tasks.Open'),
            'assigned' => trans('tasks.Assigned'),
            'done' => trans('tasks.Done'),
            'closed' => trans('tasks.Closed'),
        ];
        return $rows;
    }

    public function getStatusValueAttribute() {
        return $this->getStatusValues()[$this->status];
    }

    public function section() {
        return $this->belongsTo(Section::class, 'section_id')->withTrashed()->withDefault();
    }

    public function creator() {
        return $this->belongsTo(User::class, 'created_by')->withTrashed()->withDefault();
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id')->withTrashed()->withDefault();
    }

    public function currency() {
        return $this->belongsTo(Currency::class, 'currency_id')->withTrashed()->withDefault();
    }

    public function offer() {
        return $this->belongsTo(Offer::class, 'offer_id')->withTrashed()->withDefault();
    }

    public function offers() {
        return $this->hasMany(Offer::class)->where('reply_to', NULL);
    }

    public function questions() {
        return $this->hasMany(Question::class);
    }

    public function rates() {
        return $this->morphMany(Rate::class, 'rateable');
    }

    public function scopeOwn($query) {
        return $query->where('created_by', '=', auth()->user()->id);
    }

    public function scopeOpen($query) {
        return $query->where('status', '=', 'open');
    }

    public function scopeClosed($query) {
        return $query->where('status', '=', 'closed');
    }

    public function getData() {
        return $this->with(['creator', 'section', 'country','offers'])
                        ->when(request('status'), function($q) {
                            return $q->where('status', request('status'));
                        })
                        ->when(request('section_id'), function($q) {
                            return $q->where('section_id', request('section_id'));
                        })
                        ->when(request('where'), function($q) {
                            return $q->where('where', request('where'));
                        })
                        ->when(request('time'), function($q) {
                            return $q->where('time', request('time'));
                        })
                        ->when(request('budget'), function($q) {
                            return $q->where('budget', '<=', request('budget'));
                        })
                        ->when(request('country_id'), function($q) {
                            return $q->where('country_id', request('country_id'));
                        })
                        ->when(request('from_date'), function($q) {
                            return $q->where('date', '>=', request('from_date'));
                        })->when(request('to_date'), function($q) {
                            return $q->where('date', '<', date('Y-m-d', strtotime(request('to_date') . ' + 1 day')));
                        })
                        ->when(request('tags'), function($q) {
                            return $q->where('tags', 'LIKE', '%' . request('tags') . '%');
                        })
                        ->when(request('q'), function($q) {
                            return $q->where('title', 'LIKE', '%' . request('q') . '%');
                        });
    }

    public function export($rows, $fileName) {
        return (new \Rap2hpoutre\FastExcel\FastExcel($rows))
                        ->download($fileName . "_" . date("Y-m-d H:i:s") . '.xlsx', function ($row) {
                            $object['id'] = $row->id;
                            $object['Title'] = @$row->title;
                            $object['Section'] = @$row->section->title;
                            $object['Content'] = @$row->content;
                            $object['tags'] = @$row->tags;
                            $object['meta_description'] = @$row->meta_description;
                            $object['meta_keywords'] = @$row->meta_keywords;
                            $object['Created at'] = $row->created_at;
                            $object['Creator'] = $row->creator->name;
                            return $object;
                        });
    }

    public function getLinkAttribute() {
        return app()->make("url")->to('/') . '/' . lang() . '/tasks/details/' . $this->id . '/' . $this->slug;
    }

    public function getTitleLimitedAttribute() {
        return str_limit($this->title, 35);
    }

    public function getContentLimitedAttribute() {
        return str_limit(strip_tags($this->content), 60);
    }

}
