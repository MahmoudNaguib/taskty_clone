<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    protected $table = "users";
    protected $guarded = [
        'deleted_at',
        'logged_user',
        'image',
    ];
    protected $hidden = [
        'password',
        'remember_token',
        'confirm_token',
        'confirmed',
        'is_active',
        'created_by',
        'updated_at',
        'deleted_at',
        'language'
    ];
    protected $appends = ['is_super_admin', 'rate'];
    ///////////////////////////// has attach
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,240x180', 'large' => 'resize,720x540'],
            'path' => 'uploads'
        ],
    ];
    public $rules = [
        'name' => 'required',
        'email' => 'required|email|unique:users,email',
        'mobile' => 'required|mobile|unique:users,mobile',
        'password' => "required|confirmed|min:8",
        'image' => 'nullable|image|max:4000',
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'mobile' => $this->mobile,
        ];
        return $array;
    }

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage($row->name, ['small' => '240x180', 'large' => '720x540']);
                \DB::table('users')->where('id', $row->id)->update(['image' => $image]);
            }
            \App\Jobs\SendRegisterEMail::dispatch($row);
        });
    }

    public function getCountries() {
        return \App\Models\Country::get(['id', 'iso', 'title'])->pluck('name', 'id');
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id')->withTrashed()->withDefault();
    }

    public function getIsSuperAdminAttribute() {
        return ($this->role_id == 1);
    }

    public function getTitleAttribute() {
        return $this->name;
    }

    public function notifications() {
        return $this->hasMany(Notification::class, 'to_id');
    }

    public function role() {
        return $this->belongsTo(Role::class, 'role_id')->withTrashed()->withDefault();
    }

    public function tokens() {
        return $this->hasMany(Token::class);
    }

    public function tasks() {
        return $this->hasMany(Task::class, 'created_by');
    }

    public function offers() {
        return $this->hasMany(Offer::class, 'created_by');
    }

    public function questions() {
        return $this->hasMany(Question::class, 'created_by');
    }

    public function favourites() {
        return $this->hasMany(Favourite::class)->with(['task']);
    }

    public function getGenders() {
        $rows = [
            'm' => trans('users.Male'),
            'f' => trans('users.Femal')
        ];
        return $rows;
    }

    public function getLanguages() {
        $rows = [
            'en' => trans('app.English'),
            'fr' => trans('app.French'),
            'ar' => trans('app.Arabic'),
            'zh' => trans('app.Chinese'),
            'cs' => trans('app.Czech'),
            'da' => trans('app.Danish'),
            'nl' => trans('app.Dutch'),
            'de' => trans('app.German'),
            'el' => trans('app.Greek'),
            'he' => trans('app.Hebrew'),
            'hu' => trans('app.Hungarian'),
            'ga' => trans('app.Irish'),
            'it' => trans('app.Italian'),
            'ja' => trans('app.Japanese'),
            'la' => trans('app.Latin'),
            'ru' => trans('app.Russian'),
        ];
        return $rows;
    }

    public function getLanguagesSpokenFieldsAttribute() {
        $fields = $this->languages_spoken;
        $str = '';
        if ($fields) {
            foreach ($fields as $f) {
                $str .= @$this->getLanguages()[$f] . ', ';
            }
        }
        return $str;
    }

    public function getLanguagesSpokenAttribute($value) {
        return json_decode($value);
    }

    public function setLanguagesSpokenAttribute($value) {
        if ($value) {
            $this->attributes['languages_spoken'] = json_encode($value);
        }
    }

    public function getTransportationFieldsAttribute() {
        $fields = $this->transportation;
        $str = '';
        if ($fields) {
            foreach ($fields as $f) {
                $str .= @$this->getTransportations()[$f] . ', ';
            }
        }
        return $str;
    }

    public function getTransportationAttribute($value) {
        return json_decode($value);
    }

    public function setTransportationAttribute($value) {
        if ($value) {
            $this->attributes['transportation'] = json_encode($value);
        }
    }

    public function getTransportations() {
        $rows = [
            'Car' => trans('app.Car'),
            'Bicycle' => trans('app.Bicycle'),
            'Scooter' => trans('app.Scooter'),
            'Truck' => trans('app.Truck'),
            'Walk' => trans('app.Walk'),
        ];
        return $rows;
    }

    public function getRoles() {
        return \App\Models\Role::where('id', '!=', 1)->pluck('title', 'id');
    }

    public function setPasswordAttribute($value) {
        if (trim($value)) {
            $this->attributes['password'] = bcrypt(trim($value));
        }
    }

    public function setConfirmTokenAttribute($value) {
        if (trim($value)) {
            $this->attributes['confirm_token'] = md5(trim($value)) . md5(time());
        }
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1)->where('confirmed', 1);
    }

    public function scopeIsTasker($query) {
        return $query->where('is_tasker', '=', 1)->where('confirmed', 1);
    }

    public function getData() {
        return $this->with(['role', 'creator']);
    }

    public function rates() {
        return $this->hasMany(Rate::class, 'user_id');
    }

    public function getrateAttribute() {
        if ($this->rates->count() > 0)
            return floor($this->rates->sum('value') / $this->rates->count());
        else
            return 0;
    }

    public function getOwnAttribute() {
        return \App\Models\Task::where('created_by', auth()->user()->id)->pluck('id')->toArray();
    }

    public function getAppliedAttribute() {
        return \App\Models\Offer::where('created_by', auth()->user()->id)->pluck('task_id')->toArray();
    }

    public function export($rows, $fileName) {
        return (new \Rap2hpoutre\FastExcel\FastExcel($rows))
            ->download($fileName . "_" . date("Y-m-d H:i:s") . '.xlsx', function ($row) {
                $object['id'] = $row->id;
                $object['Name'] = $row->name;
                $object['Email'] = $row->email;
                $object['Mobile'] = $row->mobile;
                $object['type'] = $row->type;
                $object['Created at'] = $row->created_at;
                return $object;
            });
    }

    public function getBioLimitedAttribute() {
        return str_limit(strip_tags($this->bio), 60);
    }

    public function getLinkAttribute() {
        $slug = slug($this->name);
        return app()->make("url")->to('/') . '/' . lang() . '/bio/details/' . $this->id . '/' . $slug;
    }

}
