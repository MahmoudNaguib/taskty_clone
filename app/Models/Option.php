<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Option extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \Spatie\Translatable\HasTranslations;

    ///////////////////////////// has translation
    public $translatable = ['title'];
    protected $table = "options";
    protected $guarded = [
        'deleted_at',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'type' => 'required',
        'title' => 'required',
    ];

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

    /////////////////////// Options
    public function getOptionTypes() {
        return config('option_types');
    }

    public function getData() {
        return $this;
    }

    public function export($rows, $fileName) {
        return (new \Rap2hpoutre\FastExcel\FastExcel($rows))
                        ->download($fileName . "_" . date("Y-m-d H:i:s") . '.xlsx', function ($row) {
                            $object['id'] = $row->id;
                            $object['Type'] = $row->type;
                            $object['Title'] = $row->title;
                            $object['Created at'] = $row->created_at;
                            return $object;
                        });
    }

}
