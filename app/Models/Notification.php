<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends \App\Models\BaseModel {

    protected $table = "notifications";
    protected $guarded = [
        'deleted_at',
        'logged_user',
    ];
    protected $hidden = [
        'deleted_at',
    ];

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if ($row->email_notify) {
                \App\Jobs\SendNotificationMail::dispatch($row);
            }
        });
    }

    public function from() {
        return $this->belongsTo(User::class, 'from_id')->withTrashed()->withDefault();
    }

    public function to() {
        return $this->belongsTo(User::class, 'to_id')->withTrashed()->withDefault();
    }

    public function scopeUnreaded($query) {
        return $query->where('seen_at', NULl);
    }

    public function getData() {
        $user = request('logged_user');
        if (!$user) {
            $user = auth()->user();
        }
        return $this->where('to_id', $user->id);
    }

    public function export($rows, $fileName) {
        return (new \Rap2hpoutre\FastExcel\FastExcel($rows))
                        ->download($fileName . "_" . date("Y-m-d H:i:s") . '.xlsx', function ($row) {
                            $object['id'] = $row->id;
                            $object['Message'] = $row->message;
                            $object['URL'] = $row->url;
                            $object['Created at'] = $row->created_at;
                            return $object;
                        });
    }

}
