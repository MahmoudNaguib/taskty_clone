<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    public $translatable = ['title'];
    protected $table = "countries";
    protected $guarded = [
        'deleted_at',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'iso' => 'required|size:2',
        'title' => 'required',
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
        ];
        return $array;
    }

    public function getNameAttribute() {
        return @$this->getTranslation('title', lang()) . ' (' . @$this->attributes['iso'] . ')';
    }

    public function getData() {
        return $this;
    }

    public function export($rows, $fileName) {
        return (new \Rap2hpoutre\FastExcel\FastExcel($rows))
                        ->download($fileName . "_" . date("Y-m-d H:i:s") . '.xlsx', function ($row) {
                            $object['id'] = $row->id;
                            $object['Short code'] = $row->iso;
                            $object['Title'] = @$row->title;
                            $object['Created at'] = $row->created_at;
                            return $object;
                        });
    }

}
