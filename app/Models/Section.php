<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    ///////////////////////////// has translation
    public $translatable = ['title'];
    protected $table = "sections";
    protected $guarded = [
        'deleted_at',
        'image',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'title' => 'required',
    ];
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,236x234', 'large' => 'resize,500x500'],
            'path' => 'uploads'
        ],
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
        ];
        return $array;
    }

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage($row->title, 255);
                $data['image'] = $image;
                \DB::table('sections')->where('id', $row->id)->update($data);
            }
        });
    }

    public function getData() {
        return $this->with(['main'])->when(request('parent_id'), function($q) {
                    return $q->where('parent_id', request('parent_id'));
                });
    }

    public function tasks() {
        return $this->hasMany(Task::class, 'section_id');
    }

    public function main() {
        return $this->belongsTo(Section::class, 'parent_id');
    }

    public function getParents() {
        return \App\Models\Section::where('parent_id', NULL)->pluck('title', 'id');
    }

    public function export($rows, $fileName) {
        return (new \Rap2hpoutre\FastExcel\FastExcel($rows))
                        ->download($fileName . "_" . date("Y-m-d H:i:s") . '.xlsx', function ($row) {
                            $object['id'] = $row->id;
                            $object['Title'] = @$row->title;
                            $object['Created at'] = $row->created_at;
                            return $object;
                        });
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

}
