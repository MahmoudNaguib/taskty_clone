<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;

class Transation extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\HasAttach;

    ///////////////////////////// has translation
    protected $table = "transactions";
    protected $guarded = [
        'deleted_at',
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'offer_id' => 'required',
        'task_id' => 'required',
        'status' => 'required',
    ];

    public static function boot() {
        parent::boot();
        static::created(function ($row) {

        });
    }

    public function task() {
        return $this->belongsTo(Task::class, 'task_id');
    }

    public function offer() {
        return $this->belongsTo(Offer::class, 'offer_id');
    }

    public function creator() {
        return $this->belongsTo(User::class, 'created_by')->withTrashed()->withDefault();
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id')->withTrashed()->withDefault();
    }

    public function getData() {
        return $this->with(['task', 'offer', 'user', 'creator'])
            ->when(request('created_by'), function ($q) {
                return $q->where('created_by', request('created_by'));
            })
            ->when(request('user_id'), function ($q) {
                return $q->where('user_id', request('user_id'));
            })
            ->when(request('task_id'), function ($q) {
                return $q->where('task_id', request('task_id'));
            })
            ->when(request('offer_id'), function ($q) {
                return $q->where('offer_id', request('offer_id'));
            })
            ->when(request('amount_from'), function ($q) {
                return $q->where('amount_from', '>=', request('amount_from'));
            })
            ->when(request('amount_to'), function ($q) {
                return $q->where('amount_to', '<=', request('amount_to'));
            })
            ->when(request('from_date'), function ($q) {
                return $q->where('date', '>=', request('from_date'));
            })
            ->when(request('to_date'), function ($q) {
                return $q->where('date', '<', date('Y-m-d', strtotime(request('to_date') . ' + 1 day')));
            })
            ->when(request('status'), function ($q) {
                return $q->where('status', request('status'));
            });
    }

    public function export($rows, $fileName) {
        return (new \Rap2hpoutre\FastExcel\FastExcel($rows))
            ->download($fileName . "_" . date("Y-m-d H:i:s") . '.xlsx', function ($row) {
                $object['id'] = $row->id;
                $object['Task'] = $row->task->title;
                $object['Amount'] = $row->offer->budget . ' ' . trans('app.EUR');
                $object['Status'] = $row->status;
                $object['Creator'] = $row->creator->name;
                $object['To User'] = $row->user->name;
                $object['Created at'] = $row->created_at;
                return $object;
            });
    }


    public function scopeOwn($query) {
        return $query->where('created_by', auth()->user()->id)->orWhere('user_id', auth()->user()->id);
    }

}
