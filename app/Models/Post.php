<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;

class Post extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    ///////////////////////////// has translation
    public $translatable = ['title', 'tags', 'slug', 'content', 'meta_description', 'meta_keywords'];
    protected $table = "posts";
    protected $guarded = [
        'deleted_at',
        'image',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,240x180', 'large' => 'resize,720x540'],
            'path' => 'uploads'
        ],
    ];
    public $rules = [
        'title' => 'required',
        'slug' => 'required',
        'content' => 'required',
        'image' => 'nullable|image|max:4000'
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
        ];
        return $array;
    }

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage($row->title, ['small' => '240x180', 'large' => '720x540']);
                $data['image'] = $image;
                \DB::table('posts')->where('id', $row->id)->update($data);
            }
        });
    }

    public function scopeOwn($query) {
        return $query->where('created_by', '=', auth()->user()->id);
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

    public function getData() {
        return $this->with(['comments'])
                        ->when(request('category_id'), function($q) {
                            return $q->where('category_id', request('category_id'));
                        })->when(request('title'), function($q) {
                    return $q->where('title', 'LIKE', '%' . request('title') . '%');
                });
    }

    public function export($rows, $fileName) {
        return (new \Rap2hpoutre\FastExcel\FastExcel($rows))
                        ->download($fileName . "_" . date("Y-m-d H:i:s") . '.xlsx', function ($row) {
                            $object['id'] = $row->id;
                            $object['Title'] = $row->title;
                            $object['Content'] = $row->content;
                            $object['Tags'] = $row->tags;
                            $object['Is Active'] = ($row->is_active) ? trans('app.Yes') : trans('app.No');
                            $object['Created at'] = $row->created_at;
                            return $object;
                        });
    }

    public function getLinkAttribute() {
        return app()->make("url")->to('/') . '/' . lang() . '/blog/details/' . $this->id . '/' . $this->slug;
    }

    public function getTitleLimitedAttribute() {
        return str_limit($this->title, 35);
    }

    public function getContentLimitedAttribute() {
        return str_limit(strip_tags($this->content), 60);
    }

}
