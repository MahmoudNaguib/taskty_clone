<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;

class Offer extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach;

    ///////////////////////////// has translation
    protected $table = "offers";
    protected $guarded = [
        'deleted_at',
        'attachment',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    protected static $attachFields = [
        'attachment' => [
            'path' => 'uploads'
        ],
    ];
    public $rules = [
        'task_id' => 'required',
        'budget' => 'required',
        'content' => 'required',
        'attachment' => 'nullable|max:8000|mimes:jpeg,bmp,png,txt,xls,xlsx,docx,doc,ppt,pptx'
    ];

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            \App\Jobs\OfferCreated::dispatch($row);
        });
    }

    public function task() {
        return $this->belongsTo(Task::class, 'task_id');
    }

    public function creator() {
        return $this->belongsTo(User::class, 'created_by')->withTrashed()->withDefault();
    }

    public function reply() {
        return $this->belongsTo(Offer::class, 'reply_to');
    }

    public function getData() {
        return $this->with(['task'])
                        ->when(request('task_id'), function($q) {
                            return $q->where('task_id', request('task_id'));
                        });
    }

    public function export($rows, $fileName) {
        return (new \Rap2hpoutre\FastExcel\FastExcel($rows))
                        ->download($fileName . "_" . date("Y-m-d H:i:s") . '.xlsx', function ($row) {
                            $object['id'] = $row->id;
                            $object['Task'] = $row->task->title;
                            $object['Budget'] = $row->budget;
                            $object['Content'] = $row->content;
                            $object['Is Accepted'] = ($row->is_accepted) ? trans('app.Yes') : trans('app.No');
                            $object['Created at'] = $row->created_at;
                            return $object;
                        });
    }

    public function getContentLimitedAttribute() {
        return str_limit(strip_tags($this->content), 60);
    }

    public function scopeOwn($query) {
        return $query->where('created_by', '=', auth()->user()->id);
    }

}
