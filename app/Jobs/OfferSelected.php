<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class OfferSelected implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $row;

    public function __construct($row) {
        $this->row = $row;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        try {
            $row = $this->row;
            \App\Models\Notification::create([
                'from_id' =>$row->task->created_by,
                'to_id' => $row->created_by,
                'message' => trans('app.Your offer has been selected').',<br>' . $row->content . ',<br>' . trans('app.Budget') . ':' . $row->budget . ' ' . trans('app.EUR'),
                'url' => $row->task->link,
                'email_notify' => 1
            ]);
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

}
