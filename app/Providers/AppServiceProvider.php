<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Form;
use Validator;
use Illuminate\Http\Resources\Json\Resource;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Schema::defaultStringLength(190);
        /// validation rules
        Validator::extend('mobile', function ($attribute, $value, $parameters, $validator) {
            //return preg_match('/^[0-9\-\(\)\/\+\s]*$/', $value);
            if ($value == '') {
                return true;
            }
            if (!trim($value) && intval($value) != 0) {
                return true;
            }
            return (preg_match('/^([(+)0-9,\\-,+,]){4,16}$/', $value));
        });
        Resource::withoutWrapping();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
