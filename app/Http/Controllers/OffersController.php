<?php

namespace App\Http\Controllers;

class OffersController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct(\App\Models\Offer $model) {
        $this->model = $model;
        $this->middleware('auth');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function getSelect($id) {
        $offer = $this->model->findOrFail($id);
        if ($offer) {
            if (in_array($offer->task_id, auth()->user()->own)) {
                $offer->is_accepted = 1;
                $offer->save();
                $offer->task->offer_id = $id;
                $offer->task->status = 'assigned';
                $offer->task->save();
                \App\Jobs\OfferSelected::dispatch($offer);
                flash()->success(trans('app.Offer selected successfully'));
                return back();
            }
            else {
                flash()->error(trans('app.You are not the owner of this task'));
                return back();
            }
        }
    }

    public function postCreate() {
        $task = \App\Models\Task::open()->findOrFail(request('task_id'));
        if ($task) {
            request()->request->add(['task_id' => request('task_id')]);
            $this->validate(request(), $this->rules);
            if (!in_array($task->id, auth()->user()->applied)) {
                if (in_array($task->id, auth()->user()->own)) {
                    flash()->error(trans('app.You cannot apply to your own task'));
                    return back();
                }
                $data = [
                    'task_id' => request('task_id'),
                    'budget' => request('budget'),
                    'content' => request('content'),
                    'created_by' => auth()->user()->id
                ];
                if (\App\Models\Offer::create($data)) {
                    flash()->success(trans('app.Offer created successfully'));
                    return back();
                }
                flash()->error(trans('app.Failed to apply to this task'));
                return back();
            }
            else {
                flash()->error(trans('app.You are already applied to this task'));
                return back();
            }
        }
    }

    public function getDelete($id) {
        $row = $this->model->where('created_by', auth()->user()->id)->findOrFail($id);
        $row->delete();
        flash()->success(trans('app.Offer has been successfully'));
        return back();
    }

}
