<?php

namespace App\Http\Controllers;

class QuestionsController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct(\App\Models\Question $model) {
        $this->model = $model;
        $this->middleware('auth');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function postCreate() {
        $this->validate(request(), $this->rules);
        $task = \App\Models\Task::open()->findOrFail(request('task_id'));
        
        if ($task) {
            $data = [
                'task_id' => request('task_id'),
                'content' => request('content'),
                'created_by' => auth()->user()->id
            ];
            if (\App\Models\Question::create($data)) {
                flash()->success(trans('app.Question created successfully'));
                return back();
            }
            flash()->error(trans('app.Failed to apply to this task'));
            return back();
        }
    }

}
