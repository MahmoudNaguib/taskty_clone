<?php

namespace App\Http\Controllers;


use App\Models\Rate;

class TasksController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct(\App\Models\Task $model) {
        $this->module = 'tasks';
        $this->views = 'front.' . $this->module;
        $this->model = $model;
        $this->rules = $model->rules;
        $this->middleware('auth')->only(['getMyTasks', 'getCreate', 'postCreate', 'getEdit', 'postEdit']);
        $this->middleware('isTasker')->only(['getCreate', 'postCreate', 'getEdit', 'postEdit']);
    }

    public function getIndex() {
        $data['page_title'] = trans('app.Tasks');
        $data['module'] = $this->module;
        $data['row'] = $this->model;
        $data['rows'] = $this->model->getData()->open()->where('offer_id', NULL)->latest()->paginate(env('PAGE_LIMIT', 10));
        if (request()->all()) {
            foreach (request()->all() as $key => $value) {
                if ($value) {
                    $row = \App\Models\Search::where('key', $key)->where('value', $value)->first();
                    if ($row) {
                        $row->increment('total');
                    } else {
                        \App\Models\Search::create(['key' => $key, 'value' => $value]);
                    }
                }
            }
        }
        return view($this->views . '.index', $data);
    }

    public function getMyTasks() {
        $data['page_title'] = trans('app.My tasks');
        $data['row'] = $this->model;
        $data['module'] = $this->module;
        $data['rows'] = $this->model->getData()->own()->latest()->get();
        return view($this->views . '.my-tasks', $data);
    }

    public function getMyOffers() {
        $data['page_title'] = trans('app.My offers');
        $data['module'] = $this->module;
        $data['row'] = $this->model;
        $data['rows'] = \App\Models\Offer::with(['task','creator'])->own()->get();
        return view($this->views . '.my-offers', $data);
    }

    public function getCreate() {
        $data['page_title'] = trans('app.Create task');
        $data['row'] = $this->model;
        return view($this->views . '.create', $data);
    }

    public function postCreate() {
        $slug = slug(request('title'));
        request()->request->add(['slug' => $slug]);
        request()->request->add(['created_by' => auth()->user()->id]);
        $this->validate(request(), $this->rules);
        if ($row = $this->model->create(request()->all())) {
            flash()->success(trans('app.Created successfully'));
            return redirect(lang() . '/' . $this->module);
        }
        flash()->error(trans('app.failed to save'));
    }

    public function getEdit($id) {
        $data['page_title'] = trans('app.Edit task');
        $data['row'] = $this->model->findOrFail($id);
        return view($this->views . '.create', $data);
    }

    public function postEdit($id) {
        $slug = slug(request('title'));
        request()->request->add(['slug' => $slug]);
        request()->request->add(['created_by' => auth()->user()->id]);
        $row = $this->model->findOrFail($id);
        $this->validate(request(), $this->rules);
        if ($row->update(request()->all())) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getDelete($id) {
        $row = $this->model->where('created_by', auth()->user()->id)->findOrFail($id);
        $row->offers()->delete();
        $row->questions()->delete();
        $row->delete();
        flash()->success(trans('app.Deleted Successfully'));
        return back();
    }

    public function getDetails($id, $slug = NULL) {
        $data['row'] = $this->model->findOrFail($id);
        $data['row']->increment('views');
        $data['page_title'] = $data['row']->title;
        if (!session()->has('viewedTasks')) {
            session(['viewedTasks' => [$id]]);
        } else {
            $value = session('viewedTasks');
            array_push($value, $id);
            session(['viewedTasks' => $value]);
        }
        $data['image'] = $data['row']->image;
        $data['meta_description'] = str_limit(strip_tags($data['row']->content), 100);
        $data['meta_keywords'] = $data['row']->tags;
        return view($this->views . '.details', $data);
    }

    public function getDone($id) {
        $task = \App\Models\Task::findOrFail($id);
        if ($task->status != 'closed') {
            if ($task->offer && $task->offer->created_by == auth()->user()->id) {
                $task->status = 'done';
                $task->save();
                flash()->success(trans('app.Task status has been changed to done'));
            }
        }
        return back();
    }

    public function getClose($id) {
        $task = \App\Models\Task::own()->findOrFail($id);
        $task->status = 'closed';
        $task->save();
        flash()->success(trans('app.Task status has been changed to closed'));
        return back();
    }

    public function getRate($id) {
        $data['page_title'] = trans('app.Rate and Review');
        $data['row'] = $this->model->where('status','done')->findOrFail($id);
        $data['rate']=new \App\Models\Rate();
        return view($this->views . '.rate', $data);
    }

    public function postRate($id) {
        $row=$this->model->where('status','done')->findOrFail($id);
        if(auth()->user()->id==$row->created_by){
            $userId=$row->offer->created_by;
        }
        else{
            $userId=$row->created_by;
        }
        request()->request->add(['user_id'=>@$userId]);
        request()->request->add(['task_id'=>$row->id]);
        if (\App\Models\Rate::Create(request()->all())) {
            flash(trans('app.Created successfully'))->success();
            return redirect(lang().'/transactions');
        }
    }

}
