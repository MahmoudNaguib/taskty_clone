<?php

namespace App\Http\Controllers;

class HomeController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct() {
        $this->module = 'home';
        $this->views = 'front.' . $this->module;
    }

    public function getIndex(\App\Models\Post $post, \App\Models\Task $task, \App\Models\User $user) {
        $data['page_title'] = trans('app.Home');
        $data['posts'] = $post->getData()->active()->latest()->take(6)->get();
        $data['tasks'] = $task->getData()->latest()->take(6)->get();
        if (session()->has('viewedTasks')) {
            $data['viewedTasks'] = $task->getData()->whereIn('id', session('viewedTasks'))->latest()->take(6)->get();
        }
        if (auth()->user()) {
            $appliedSections = $task->getData()->whereIn('id', auth()->user()->applied)->pluck('section_id');
            $data['relatedTasks'] = $task->getData()->whereIn('section_id', $appliedSections)->latest()->take(6)->get();
        }
        $data['taskers'] = $user->getData()->active()->where('is_tasker', 1)->inRandomOrder()->take(6)->get();
        return view($this->views . '.index', $data);
    }

}
