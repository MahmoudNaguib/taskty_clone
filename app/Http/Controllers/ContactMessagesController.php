<?php

namespace App\Http\Controllers;

class ContactMessagesController extends \App\Http\Controllers\Controller {

    public function __construct(\App\Models\ContactMessage $model) {
        $this->model = $model;
    }

    public function postIndex() {
        $this->validate(request(), $this->model->rules);
        if ($row = $this->model->create(request()->all())) {
            flash()->success(trans('app.Sent successfully'));
            return back();
        }
        flash()->error(trans('app.failed to save'));
    }

}
