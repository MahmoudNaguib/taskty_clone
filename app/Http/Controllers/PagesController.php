<?php

namespace App\Http\Controllers;

class PagesController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct() {
        $this->module = 'pages';
        $this->views = 'front.' . $this->module;
    }

    public function about() {
        $data['page_title'] = trans('app.About us');
        $data['meta_description'] = conf('about_meta_description');
        $data['meta_keywords'] = conf('about_meta_keywords');
        $data['image'] = conf('about_banner');
        $data['content'] = conf('about_text');
        return view($this->views . '.about', $data);
    }
    public function contact() {
        $data['page_title'] = trans('app.Contact us');
        $data['meta_description'] = conf('contact_meta_description');
        $data['meta_keywords'] = conf('contact_meta_keywords');
        $data['image'] = conf('contact_banner');
        $data['content'] = conf('contact_text');
        return view($this->views . '.contact', $data);
    }
    public function terms() {
        $data['page_title'] = trans('pages.Terms and conditions');
        $data['meta_description'] = conf('terms_meta_description');
        $data['meta_keywords'] = conf('terms_meta_keywords');
        $data['image'] = conf('terms_banner');
        $data['content'] = conf('terms_text');
        return view($this->views . '.terms', $data);
    }
    public function privacy() {
        $data['page_title'] = trans('pages.Privacy policy');
        $data['meta_description'] = conf('privacy_meta_description');
        $data['meta_keywords'] = conf('privacy_meta_keywords');
        $data['image'] = conf('privacy_banner');
        $data['content'] = conf('privacy_text');
        return view($this->views . '.privacy', $data);
    }

}
