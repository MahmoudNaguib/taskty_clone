<?php

namespace App\Http\Controllers;

class PostsController extends \App\Http\Controllers\Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Post $model) {
        $this->module = 'posts';
        $this->views = 'front.' . $this->module;
        $this->title = trans('app.Blog posts');
        $this->model = $model;
        $this->rules = $model->rules;
        $this->middleware('auth');
    }

    public function getIndex() {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->own()->latest()->paginate(env('PAGE_LIMIT', 10));
        $data['row'] = $this->model;
        return view($this->views . '.index', $data);
    }

    public function getCreate() {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.Create') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model;
        return view($this->views . '.create', $data);
    }

    public function postCreate() {
        foreach (langs() as $lang) {
            $slug[$lang] = slug(request('title.' . $lang));
        }
        request()->request->add(['slug' => $slug, 'is_active' => 0]);
        $this->validate(request(), $this->rules);
        if ($row = $this->model->create(request()->all())) {
            flash()->success(trans('app.Created successfully'));
            return redirect(lang() . '/' . $this->module);
        }
        flash()->error(trans('app.failed to save'));
    }

    public function getEdit($id) {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.Edit') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->findOrFail($id);
        return view($this->views . '.edit', $data);
    }

    public function postEdit($id) {
        foreach (langs() as $lang) {
            $slug[$lang] = slug(request('title.' . $lang));
        }
        $row = $this->model->findOrFail($id);
        request()->request->add(['slug' => $slug, 'is_active' => $row->is_active]);
        $this->validate(request(), $this->rules);
        if ($row->update(request()->all())) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getView($id) {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.View') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->own()->findOrFail($id);
        return view($this->views . '.view', $data);
    }

    public function getDelete($id) {
        $row = $this->model->own()->findOrFail($id);
        $row->delete();
        flash()->success(trans('app.Deleted Successfully'));
        return back();
    }

}
