<?php

namespace App\Http\Controllers;

class DashboardController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct() {
        $this->module = 'dashboard';
        $this->views='front.'.$this->module;
    }

    public function getIndex() {
        $data['page_title'] = trans('app.My Dashbaord');
        return view($this->views . '.index', $data);
    }
}