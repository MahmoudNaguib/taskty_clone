<?php

namespace App\Http\Controllers;

class FavouritesController extends \App\Http\Controllers\Controller {

    public function __construct(\App\Models\Favourite $model) {
        $this->middleware(['auth']);
        $this->module = 'favourites';
        $this->views = 'front.' . $this->module;
        $this->model = $model;
    }

    public function getIndex(\App\Models\Task $task) {
        $data['page_title'] = trans('app.Favourite tasks');
        $data['row'] = $this->model;
        $data['module'] = $this->module;
        $favourites = auth()->user()->favourites->pluck('task_id')->toArray();
        $data['rows'] = $task->getData()->whereIn('id', $favourites)->open()->latest()->paginate(env('PAGE_LIMIT', 10));
        return view($this->views . '.index', $data);
    }

    public function getAdd($id) {
        $favourites = auth()->user()->favourites->pluck('task_id')->toArray();
        if (in_array($id, $favourites)) {
            flash()->success(trans('app.This task already added to your favourites'));
        }
        else {
            auth()->user()->favourites()->create(['task_id' => $id]);
            flash()->success(trans('app.Added successfully to my favourites'));
        }
        return back();
    }

}
