<?php

namespace App\Http\Controllers;

class AjaxController extends \App\Http\Controllers\Controller {

    public function anySort() {
        if (request('items')) {
            $u = 1;
            foreach (request('items') as $item) {
                $unit = \App\Models\Unit::find($item['id']);
                \App\Models\Unit::where('id',$item['id'])->update(['order_field'=>$u]);
                if (@$item['children']) {
                    $l = 1;
                    foreach ($item['children'][0] as $lecture) {
                        $lecture = \App\Models\Lecture::find($lecture['id']);
                        \App\Models\Lecture::where('id',$lecture['id'])->update([
                            'unit_id'=>$unit->id,
                            'order_field'=>$l
                        ]);
                        $l++;
                    }
                }
                $u++;
            }
           echo 'success';exit;
        }
    }

}
