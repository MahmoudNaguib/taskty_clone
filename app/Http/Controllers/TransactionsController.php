<?php

namespace App\Http\Controllers;

use App\Models\Transation;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\Money;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\OrderItem;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\PaymentBrand;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\PaymentBrandForce;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\ProductType;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\request\MerchantOrder;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\Address;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\CustomerInformation;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\VatCategory;


use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\Environment;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\signing\SigningKey;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\endpoint\Endpoint;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\connector\TokenProvider;

class InMemoryTokenProvider extends TokenProvider {
    private $map = array();

    /**
     * Construct the in memory token provider with the given refresh token.
     * @param string $refreshToken The refresh token used to retrieve the access tokens with.
     */
    public function __construct($refreshToken) {
        $this->setValue(static::REFRESH_TOKEN, $refreshToken);
    }

    /**
     * Retrieve the value for the given key.
     *
     * @param string $key
     * @return string Value of the given key or null if it does not exists.
     */
    protected function getValue($key) {
        return array_key_exists($key, $this->map) ? $this->map[$key] : null;
    }

    /**
     * Store the value by the given key.
     *
     * @param string $key
     * @param string $value
     */
    protected function setValue($key, $value) {
        $this->map[$key] = $value;
    }

    /**
     * Optional functionality to flush your systems.
     * It is called after storing all the values of the access token and can be used for example to clean caches or reload changes from the database.
     */
    protected function flush() {
    }
}


class TransactionsController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct(\App\Models\Transation $model) {
        $this->module = 'transactions';
        $this->views = 'front.' . $this->module;
        $this->title = trans('app.Transactions');
        $this->model = $model;
        $this->rules = $model->rules;
        $this->middleware('auth');
    }

    public function getIndex() {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->own()->latest()->get();
        $data['row'] = $this->model;
        return view($this->views . '.index', $data);
    }

    public function getDetails($id, $slug = NULL) {
        $data['row'] = $this->model->findOrFail($id);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.View transaction') . '#' . $data['row']->id;
        return view($this->views . '.details', $data);
    }

    public function getTransfer($id) {
        $row = \App\Models\Offer::findOrFail($id);
        $title = trans('app.Payment for task') . ':' . '#' . $row->task_id . ', ' . $row->task->title;
        $orderItems = [OrderItem::createFrom([
            'id' => $row->id,
            'name' => $title,
            'description' => $title,
            'quantity' => 1,
            'amount' => Money::fromDecimal('EUR', $row->budget),
            'tax' => Money::fromDecimal('EUR', 0),
            'category' => ProductType::DIGITAL,
            'vatCategory' => VatCategory::HIGH
        ])];
        /*$shippingDetail = Address::createFrom([
            'firstName' => 'Jan',
            'middleName' => 'van',
            'lastName' => 'Veen',
            'street' => 'Voorbeeldstraat',
            'postalCode' => '1234AB',
            'city' => 'Haarlem',
            'countryCode' => 'NL',
            'houseNumber' => '5',
            'houseNumberAddition' => 'a'
        ]);*/
        $name = explode(',', $row->creator->name);
        $billingDetails = Address::createFrom([
            'firstName' => @$name[0],
            'middleName' => @$name[1],
            'lastName' => @$name[2],
            'countryCode' => $row->creator->country->code,
            'city' => $row->creator->city,
            'street' => $row->creator->street_name,
            'houseNumber' => $row->creator->house_number,
            'postalCode' => $row->creator->postal_code,
        ]);
        $customerInformation = CustomerInformation::createFrom([
            'emailAddress' => $row->creator->email,
            'dateOfBirth' => date('m-d-Y', strtotime($row->creator->date_of_birth)),
            'gender' => ucfirst($row->creator->gender),
            'initials' => substr(@$name[0], 0, 1) . '.' . substr(@$name[2], 0, 1),
            'telephoneNumber' => $row->creator->mobile
        ]);
        $order = MerchantOrder::createFrom([
            'merchantOrderId' => $row->id,
            'description' => $title,
            'orderItems' => $orderItems,
            'amount' => Money::fromDecimal('EUR', $row->budget),
            // 'shippingDetail' => $shippingDetail,
            'billingDetail' => $billingDetails,
            'customerInformation' => $customerInformation,
            'language' => $row->creator->language,
            'merchantReturnURL' => env('APP_URL') . '/transactions/redirect/' . encrypt($row->id),
            'paymentBrand' => PaymentBrand::IDEAL,
            'paymentBrandForce' => PaymentBrandForce::FORCE_ONCE
        ]);
        if(env('OMNIPAY_ENVIRONMENT')=='PRODUCTION'){
            $endpoint = Endpoint::createInstance(
                ENVIRONMENT::PRODUCTION, // ENVIRONMENT::SANDBOX, PRODUCTION
                new SigningKey(base64_decode(env('SIGNINKEY'))),
                new InMemoryTokenProvider(env('REFRESH_TOKEN'))
            );
        }
        else{
            $endpoint = Endpoint::createInstance(
                ENVIRONMENT::SANDBOX, // ENVIRONMENT::SANDBOX, PRODUCTION
                new SigningKey(base64_decode(env('SIGNINKEY'))),
                new InMemoryTokenProvider(env('REFRESH_TOKEN'))
            );
        }

        $redirectUrl = $endpoint->announceMerchantOrder($order);
// Redirect user to Rabo OmniKassa
        header('Location: ' . $redirectUrl);
        die();
    }

    public function getRedirect($id) {
        $id = decrypt($id);
        $offer = \App\Models\Offer::findOrFail($id);
        $data = [
            'offer_id' => $offer->id,
            'task_id' => $offer->task_id,
            'signature' => request('signature'),
            'amount' => $offer->budget,
            'status' => request('status'),
            'created_by' => auth()->user()->id,
            'user_id' => $offer->created_by
        ];
        if ($row = Transation::create($data)) {
            if ($row->status == 'COMPLETED') {
                $offer->is_paid = 1;
                $offer->save();
                $offer->task->status = 'done';
                $offer->task->save();
                flash(trans('app.Payment successfully'))->success();
                return redirect(lang() . '/transactions/details/' . $row->id);
            } else {
                flash(trans('app.Payment failed'))->error();
                return redirect(lang() . '/transactions/details/' . $row->id);
            }
        }
    }
}
