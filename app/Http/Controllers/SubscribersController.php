<?php

namespace App\Http\Controllers;

class SubscribersController extends Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Subscriber $model) {
        $this->model = $model;
    }

    public function postIndex() {
        request()->request->add(['name'=>  request('email')]);
        $this->validate(request(), $this->model->rules);
        if ($row = $this->model->create(request()->all())) {
            flash()->success(trans('app.Sent successfully'));
            return back();
        }
        flash()->error(trans('app.failed to save'));
    }

}
