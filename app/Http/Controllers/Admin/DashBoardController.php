<?php

namespace App\Http\Controllers\Admin;

class DashBoardController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct() {
        $this->module = 'admin/dashboard';
    }

    public function getIndex(\App\Models\Task $task) {
        $data['page_title'] = trans('app.Dashboard');
        $data['sections'] = \App\Models\Section::pluck('title', 'id')->toArray();
        $data['countries'] = \App\Models\Country::pluck('title', 'id')->toArray();
        $data['times'] = $task->getTimeValues();
        $data['where'] = $task->getWhereValues();
        $data['top_search_sections'] = \App\Models\Search::where('key', 'section_id')->orderBy('total', 'desc')->limit(5)->get();
        $data['top_search_countries'] = \App\Models\Search::where('key', 'country_id')->orderBy('total', 'desc')->limit(5)->get();
        $data['top_search_times'] = \App\Models\Search::where('key', 'time')->orderBy('total', 'desc')->limit(5)->get();
        $data['top_search_where'] = \App\Models\Search::where('key', 'where')->orderBy('total', 'desc')->limit(5)->get();
        $data['top_search_budget'] = \App\Models\Search::where('key', 'budget')->orderBy('total', 'desc')->limit(5)->get();
        $data['pendingPosts'] = \App\Models\Post::where('is_active',0)->get();
        return view($this->module . '.index', $data);
    }

}
