<?php

namespace App\Http\Controllers\Admin;

class AjaxController extends \App\Http\Controllers\Controller {
    public $model;
    public $module;

    public function __construct() {
        $this->middleware(['auth','isAdmin']);
    }
}