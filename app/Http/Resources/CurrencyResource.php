<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        foreach (langs() as $lang) {
            $record[$lang] = $this->getTranslation('title', $lang);
        }
        return [
            'type' => 'currencies',
            'id' => $this->id,
            'attributes' => [
                'iso'=>$this->when(isset($this->iso), $this->iso),
                'title' => $record,
                'rate'=>$this->when(isset($this->rate), $this->rate),
            ],
            'relationships' => [
                'creator'=>new UserResource($this->whenLoaded('creator')),
            ]
        ];
    }

}
