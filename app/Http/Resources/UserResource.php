<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'type'=>'users',
            'id'=>$this->id,
            'attributes'=>[
                'role_id'=>$this->when(isset($this->role_id), $this->role_id),
                'gender'=>$this->when(isset($this->gender), $this->gender),
                'name'=>$this->when(isset($this->name), $this->name),
                'email'=>$this->when(isset($this->email), $this->email),
                'mobile'=>$this->when(isset($this->mobile), $this->mobile),
                'language'=>$this->when(isset($this->language), $this->language),
                'image'=>$this->when(isset($this->image), $this->image),
                'last_logged_in_at'=>$this->when(isset($this->last_logged_in_at), $this->last_logged_in_at),
                'last_ip'=>$this->when(isset($this->last_ip), $this->last_ip),
            ],
            'relationships'=>[
                'creator'=>new UserResource($this->whenLoaded('creator')),
            ]
        ];
    }
}