<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ConfigsResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        $output = [
            'type' => 'configs',
            'id' => $this->id,
            'attributes' => [
                'lang'=>$this->when(isset($this->lang), $this->lang),
                'type'=>$this->when(isset($this->type), $this->type),
                'label'=>$this->when(isset($this->label), $this->label),
                'field'=>$this->when(isset($this->field), $this->field),
                'value'=>$this->when(isset($this->value), $this->value),
                
            ],
            'relationships' => [
            ]
        ];
        return $output;
    }

}
