<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Post::class, function (Faker $faker) {
    foreach (langs() as $lang) {
        $title[$lang] = $faker->sentence(4);
        $tags[$lang] = $faker->sentence(1).rand(1,10);
        $slug[$lang] = slug($title[$lang]);
        $content[$lang] = $faker->paragraph();
    }
    return [
        'title' => $title,
        'slug' => $slug,
        'content' => $content,
        'tags'=>$tags,
        'is_active' => 1,
        'created_by' => 2,
        'views'=>rand(1,1000)
    ];
});
