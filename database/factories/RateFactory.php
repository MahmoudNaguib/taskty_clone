<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Rate::class, function (Faker $faker) {
    $itemIds = App\Models\Task::pluck('id')->toArray();
    $itemType = 'App\Models\Task';
    $usersIds = App\Models\User::pluck('id')->toArray();
    return [
        'rateable_id' => $itemIds[array_rand($itemIds)],
        'rateable_type' => $itemType,
        'created_by' => $usersIds[array_rand($usersIds)],
        'value' => rand(1, 5)
    ];
});
