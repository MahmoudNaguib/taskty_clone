<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Course::class, function (Faker $faker) {
    foreach (langs() as $lang) {
        $title[$lang] = $faker->sentence(4);
        $slug[$lang] = slug($title[$lang]);
        $content[$lang] = $faker->paragraph();
    }
    $sectionsIds = App\Models\Section::where('parent_id', '!=', NULL)->pluck('id')->toArray();
    $levelsIds = \App\Models\Option::where('type', 'levels')->pluck('id')->toArray();
    $languagesIds = \App\Models\Option::where('type', 'languages')->pluck('id')->toArray();
    return [
        'section_id' => $sectionsIds[array_rand($sectionsIds)],
        'price'=>rand(100,1000),
        'level_id' => $levelsIds[array_rand($levelsIds)],
        'language_id' => $languagesIds[array_rand($languagesIds)],
        'title' => $title,
        'slug' => $slug,
        'content' => $content,
        'is_active' => 1,
        'created_by' => 2,
        'views' => rand(1, 1000)
    ];
});
