<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Faq::class, function (Faker $faker) {
    foreach (langs() as $lang) {
        $title[$lang] = $faker->sentence(4);
        $content[$lang] = $faker->paragraph();
    }
    return [
        'title' => $title,
        'content' => $content,
        'is_active' => 1,
        'created_by' => 2,
    ];
});
