<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Offer::class, function (Faker $faker) {
    $tasksIds = App\Models\Task::pluck('id')->toArray();
    $createdByIds= App\Models\User::where('type','guest')->where('is_tasker',0)->pluck('id')->toArray();
    return [
        'task_id' => $tasksIds[array_rand($tasksIds)],
        'content' => $faker->paragraph(),
        'budget' => rand(5,100),
        'created_by' => $createdByIds[array_rand($createdByIds)],
    ];
});
