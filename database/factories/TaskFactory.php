<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Task::class, function (Faker $faker) {
    $task = new \App\Models\Task();
    $sectionIds = App\Models\Section::where('parent_id', '!=', NULL)->pluck('id')->toArray();
    $whereValues = array_keys($task->getWhereValues());
    $timeValues = array_keys($task->getTimeValues());
    $statusValues = array_keys($task->getStatusValues());
    $title = $faker->sentence(4);
    $slug = slug($title);
    $tags = 'tag ' . rand(1, 10);
    $taskerUsers = App\Models\User::where('is_tasker', 1)->pluck('id')->toArray();
    return [
        'section_id' => $sectionIds[array_rand($sectionIds)],
        'title' => $title,
        'slug' => $slug,
        'content' => $faker->paragraph(),
        'where' => $whereValues[array_rand($whereValues)],
        'country_id' => 64,
        'location' => $faker->address(),
        'longitude' => rand(28, 31) . '.' . rand(1000, 1000000),
        'latitude' => rand(28, 31) . '.' . rand(1000, 1000000),
        'date' => date('Y-m-d', strtotime(date("Y-m-d", strtotime("tomorrow")) . ' + ' . rand(1, 30) . ' days')),
        'time' => $timeValues[array_rand($timeValues)],
        'budget' => rand(5, 100),
        'tags' => $tags,
        'status' => 'open',
        'views' => rand(1, 1000),
        'created_by' => $taskerUsers[array_rand($taskerUsers)],
        'views' => rand(1, 1000)
    ];
});
