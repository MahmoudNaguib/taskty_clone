<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    $itemIds = App\Models\Post::pluck('id')->toArray();
    $itemType = 'App\Models\Post';
    $usersIds = App\Models\User::pluck('id')->toArray();
    return [
        'commentable_id' => $itemIds[array_rand($itemIds)],
        'commentable_type' => $itemType,
        'content' => $faker->paragraph(),
        'created_by' => $usersIds[array_rand($usersIds)]
    ];
});
