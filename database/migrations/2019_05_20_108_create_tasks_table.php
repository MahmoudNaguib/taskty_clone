<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskstable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('section_id')->nullable()->index(); //required
            $table->bigInteger('offer_id')->nullable()->index();
            $table->string('title')->nullable(); // required
            $table->string('slug')->nullable();
            $table->text('content')->nullable();
            $table->enum('where', ['in_person', 'remote'])->nullable()->default('in_person'); // options
            $table->bigInteger('country_id')->nullable()->index();
            $table->string('location')->nullable();
            $table->decimal('longitude', 11, 8)->nullable()->index();
            $table->decimal('latitude', 11, 8)->nullable()->index();
            $table->date('date')->nullable()->index();
            $table->enum('time', ['before_10am', '10am_2pm', 'after_2pm_6pm', 'after_6pm'])->nullable(); //options
            $table->float('budget', 11, 2)->index()->default(0);
            $table->text('tags')->nullable();
            $table->enum('status', ['draft', 'open', 'assigned','done','closed'])->default('draft')->index();
            $table->string('attachment')->nullable();
            $table->string('image')->nullable();
            $table->bigInteger('views')->nullable()->index()->default(0);
            $table->bigInteger('created_by')->nullable()->index();
            $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deleted_at')->nullable()->index();
        });
        if (app()->environment() != 'testing') {
            Schema::table('tasks', function (Blueprint $table) {
                \DB::statement('ALTER TABLE tasks ADD FULLTEXT search(title)');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tasks');
    }

}
