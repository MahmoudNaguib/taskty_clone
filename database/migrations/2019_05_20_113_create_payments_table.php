<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('task_id')->nullable()->index()->default(NULL);
            $table->bigInteger('offer_id')->nullable()->index()->default(NULL);
            $table->bigInteger('from_id')->nullable()->index()->default(NULL);
            $table->bigInteger('to_id')->nullable()->index()->default(NULL);
            $table->bigInteger('currency_id')->nullable()->index()->default(NULL);
            $table->float('amount', 11, 2)->index()->default(0);
            $table->enum('status',['pending','collected','requested','delivered'])->nullable()->default('pending'); // options
            $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deleted_at')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('payments');
    }

}
