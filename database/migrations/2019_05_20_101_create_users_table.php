<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->nullable()->default('guest')->index();
            $table->bigInteger('role_id')->nullable()->index(); // required if user is admin
            $table->string('name')->nullable()->index(); // required
            $table->string('email')->nullable()->index(); //required|emaill
            $table->string('mobile')->nullable()->index(); //required|mobile
            $table->string('password')->nullable(); //required | min 8 chars
            $table->string('language', 2)->nullable()->default('en')->index();
            $table->enum('gender', ['m', 'f'])->nullable()->default('m'); // options
            $table->string('date_of_birth')->nullable();
            $table->string('image', 190)->nullable();
            $table->string('remember_token', 190)->nullable();
            $table->string('confirm_token', 190)->nullable();
            $table->string('last_ip', 190)->nullable();
            $table->timestamp('last_logged_in_at')->nullable();
            $table->boolean('confirmed')->nullable()->default(1);
            $table->boolean('is_active')->nullable()->default(1)->index();
            $table->boolean('is_default')->nullable()->default(0)->index();
            $table->text('bio')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->boolean('is_tasker')->nullable()->default(0)->index();
            $table->string('languages_spoken')->nullable()->index(); // English'en', French'fr', Arabic'ar', Chinese'zh', Czech'cs', Danish'da', Dutch'nl', German'de', Greek'el', Hebrew'he',Hungarian'hu', Irish'ga', Italian'it', Japanese'ja', Latin'la',Russian'ru'
            $table->string('transportation')->nullable()->index(); //Bicycle, Car, Scooter, Truck, Walk
            $table->string('qualifications')->nullable();
            $table->text('work_experience')->nullable();
            $table->boolean('id_badge')->nullable()->default(0)->index();
            $table->boolean('license_badge')->nullable()->default(0)->index();
            $table->boolean('partner_badge')->nullable()->default(0)->index();
            $table->boolean('working_with_children_badge')->nullable()->default(0)->index();
            $table->string('bank_name')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('swift_code')->nullable();
            ///////////////// Address information
            $table->bigInteger('country_id')->nullable()->default(env('COUNTRY_ID'))->index();
            $table->string('city')->nullable()->index();
            $table->string('street_name')->nullable()->index();
            $table->string('house_number')->nullable()->index();
            $table->string('postal_code')->nullable()->index();
            ///////////////////////////////////
            $table->bigInteger('created_by')->nullable()->index();
            $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deleted_at')->nullable()->index();
        });
        if (app()->environment() != 'testing') {
            Schema::table('users', function (Blueprint $table) {
                \DB::statement('ALTER TABLE users ADD FULLTEXT search(type,name,email,mobile)');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}
