<?php

use Illuminate\Database\Seeder;

class SubscribersSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (app()->environment() != 'production') {
            \DB::table('subscribers')->delete();
            \DB::statement("ALTER TABLE subscribers AUTO_INCREMENT = 1");
        }
        factory(App\Models\Subscriber::class,10)->create();
    }

}
