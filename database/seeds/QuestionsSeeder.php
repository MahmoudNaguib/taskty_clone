<?php

use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (app()->environment() != 'production') {
            \DB::table('questions')->delete();
            \DB::statement("ALTER TABLE questions AUTO_INCREMENT = 1");
        }
        $tasks = \App\Models\Task::where('status', 'open')->pluck('id')->toArray();
        if ($tasks) {
            foreach ($tasks as $taskId) {
                $createdByIds = App\Models\User::where('type', 'guest')->where('is_tasker', 0)->pluck('id')->toArray();
                if ($createdByIds) {
                    $data = [
                        'task_id' => $taskId,
                        'content' => 'Content ' . rand(1000, 100000),
                        'created_by' => $createdByIds[array_rand($createdByIds)],
                    ];
                    \App\Models\Question::create($data);
                }
            }
        }
    }

}
