<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (app()->environment() != 'production') {
            DB::table('users')->delete();
            DB::statement("ALTER TABLE users AUTO_INCREMENT =1");
            insertDefaultUsers();
        }
    }

}
