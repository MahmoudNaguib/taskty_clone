<?php

use Illuminate\Database\Seeder;

class ContactMessagesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (app()->environment() != 'production') {
            \DB::table('contact_messages')->delete();
            \DB::statement("ALTER TABLE contact_messages AUTO_INCREMENT = 1");
        }
        factory(App\Models\ContactMessage::class, 2)->create();
    }

}
