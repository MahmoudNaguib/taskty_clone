<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call(BasicSeeder::class);
        if (app()->environment() != 'production') {
           // $this->call(NotificationsSeeder::class);
            $this->call(PostsSeeder::class);
            $this->call(FaqsSeeder::class);
            $this->call(ContactMessagesSeeder::class);
            $this->call(SubscribersSeeder::class);
            $this->call(SectionsSeeder::class);
            $this->call(TasksSeeder::class);
            $this->call(QuestionsSeeder::class);
            $this->call(OffersSeeder::class);
        }
    }

}
