<?php
use Illuminate\Database\Seeder;

class FaqsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('faqs')->delete();
            DB::statement("ALTER TABLE faqs AUTO_INCREMENT = 1");
            factory(App\Models\Faq::class, 10)->create();
        }
    }
}