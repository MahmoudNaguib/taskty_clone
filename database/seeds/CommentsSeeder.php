<?php
use Illuminate\Database\Seeder;

class CommentsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('comments')->delete();
            DB::statement("ALTER TABLE comments AUTO_INCREMENT = 1");
            factory(App\Models\Comment::class, 50)->create();
        }
    }
}