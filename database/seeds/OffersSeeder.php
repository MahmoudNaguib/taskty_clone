<?php

use Illuminate\Database\Seeder;

class OffersSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (app()->environment() != 'production') {
            \DB::table('offers')->delete();
            \DB::statement("ALTER TABLE offers AUTO_INCREMENT = 1");
        }
        $tasks = \App\Models\Task::where('status', 'open')->pluck('id')->toArray();
        if ($tasks) {
            foreach ($tasks as $taskId) {
                $createdByIds = App\Models\User::where('type', 'guest')->where('is_tasker', 0)->pluck('id')->toArray();
                if ($createdByIds) {
                    $data = [
                        'task_id' => $taskId,
                        'content' => 'Content ' . rand(1000, 100000),
                        'budget' => rand(5, 100),
                        'created_by' => $createdByIds[array_rand($createdByIds)],
                    ];
                    \App\Models\Offer::create($data);
                }
            }
        }
    }

}
