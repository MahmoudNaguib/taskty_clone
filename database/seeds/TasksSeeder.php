<?php

use Illuminate\Database\Seeder;

class TasksSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (app()->environment() != 'production') {
            \DB::table('tasks')->delete();
            \DB::statement("ALTER TABLE tasks AUTO_INCREMENT = 1");
        }
        factory(App\Models\Task::class,5)->create();
    }

}
