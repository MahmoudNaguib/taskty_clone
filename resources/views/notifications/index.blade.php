@extends('layouts.auth')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if (!$rows->isEmpty())
    <div class="table-responsive">
        <table class="table display responsive nowrap dataTable">
            <thead>
                <tr>
                    <th class="wd-15p">{{trans('notifications.ID')}} </th>
                    <th class="wd-15p">{{trans('notifications.Message')}} </th>
                    <th class="wd-15p">{{trans('notifications.URL')}} </th>
                    <th class="wd-15p">{{trans('notifications.Created at')}}</th>
                    <th class="wd-15p">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rows as $row)
                <tr>
                    <td class="center">{{$row->id}}</td>
                    <td class="center">{{str_limit($row->message,60)}}</td>
                    <td class="center"><a href="{{$row->url}}" target="_blank">{{str_limit($row->url,20)}}</a></td>
                    <td class="center">{{$row->created_at}}</td>
                    <td class="center">
                        <a class="btn btn-primary btn-xs" href="{{$module}}/view/{{$row->id}}" title="{{trans('notifications.View')}}">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a class="btn btn-danger btn-xs" href="{{$module}}/delete/{{$row->id}}" title="{{trans('daycares.Delete')}}" data-confirm="{{trans('notifications.Are you sure you want to delete this item')}}?">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @else
    {{trans("notifications.There is no results")}}
    @endif
</div>
@endsection
