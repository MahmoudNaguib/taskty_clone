@extends('layouts.auth')
@section('content')
<div class="signin-box">
    {!! Form::open(['method' => 'post'] ) !!}
    {{ csrf_field() }}
    <div class="form-group">
        @php $input = 'username';
        @endphp {!! Form::text($input,request($input),['class'=>'form-control','required'=>'required','placeholder'=>trans('app.Enter your username or mobile')]) !!} @if(@$errors) @foreach($errors->get($input) as $message)
        <span class='help-inline text-danger'>{{ $message }}</span> @endforeach @endif
    </div>
    <!-- form-group -->
    <div class="form-group">
        @php $input = 'password'; @endphp
        {!! Form::password($input,['class'=>'form-control','required'=>'required','placeholder'=>trans('app.Enter your password')])!!}
        @if(@$errors) 
        @foreach($errors->get($input) as $message)
        <span class='help-inline text-danger'>{{ $message }}</span> 
        @endforeach 
        @endif
    </div>
    <div class="form-group">
        <label class="ckbox">
            <input type="checkbox" name="remember_me"><span>{{trans('app.Remember me')}}</span>
        </label>
    </div>
    
    
    <!-- form-group -->
    <button class="btn btn-primary btn-block btn-signin">{{ trans('app.Submit') }}</button>

    <p class="mg-b-0">
        <a href="{{lang()}}/auth/forgot-password">{{ trans('app.Forgot password') }}</a>
        |
        <a href="{{lang()}}/auth/register">{{ trans('app.Register') }}</a>
    </p>
    {!! Form::close() !!}
</div>
@endsection
