@extends('emails.master')

@section('title'){{trans("app.Registertion")}}@endsection

@section('content')

<p>{{trans("app.Welcome")}} <strong>{{$row->name}}</strong></p>
    <p>{{trans("app.Thanks for joining us at")}} {{ appName() }}</p>
    <p>
        {{trans("app.Here is your account details")}}
    </p>
    <p>
        @if($row->name)
        <strong>{{trans("app.Name")}} : </strong> {{$row->name}} <br>
        @endif

        @if($row->email)
        <strong>{{trans("app.Email")}} : </strong> {{$row->email}} <br>
        @endif

        @if($row->mobile)
            <strong>{{trans("app.Mobile")}} : </strong> {{$row->mobile}} <br>
        @endif

        @if(!$row->confirmed)
            <p>{{trans('email.To activate your account please click the link below')}}</p>
            <a href="{{App::make("url")->to('/')}}/auth/confirm/{{$row->confirm_token}}">{{App::make("url")->to('/')}}/auth/activate/{{ $row->confirm_token }}</a>
        @endif
    </p>
@endsection
