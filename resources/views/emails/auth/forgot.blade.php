@extends('emails.master')

@section('title'){{trans("app.Forgot your password")}}@endsection

@section('content')

<p>{{trans("app.Welcome")}} <strong>{{$row->name}}</strong></p>
<p>{{trans("app.Thanks for joining us at")}} {{ appName() }}</p>
<p>
    {{trans("app.Here is your account details")}}
</p>
<p>
    @if($row->name)
    <strong>{{trans("app.Name")}} : </strong> {{$row->name}} <br>
    @endif

    @if($row->email)
    <strong>{{trans("app.Email")}} : </strong> {{$row->email}} <br>
    @endif

    @if($password)
    <strong>{{trans("app.New Password")}} : </strong> {{@$password}} <br>
    @endif
    
</p>
@endsection
