@extends('emails.master')

@section('title'){{trans('app.New Notification From') . " " . appName() }} @endsection

@section('content')
<h2>{{trans("app.New Notification Has Been Received")}}</h2>
<p>
    <label>
        <strong>{{trans("app.Dear")}} {{$row->to->name}}</strong>
    </label>
    <br>
<p>
    {!!$row->message!!}
</p>
<br>
<a href="{{ url($row->url) }}">{{ trans('email.Check it') }}</a>
</p>
@endsection
