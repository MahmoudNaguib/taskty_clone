@extends('emails.master')

@section('title'){{trans('email.Contact us submit form')}}:{{$row->name}} @endsection

@section('content')
<h2>{{trans('email.Contact us submit form')}} : {{$row->name}}</h2>
<p>
    <strong>{{trans("app.Name")}} </strong> :  {{$row->name}}
<p>
 <p>
    <strong>{{trans("app.Mobile")}} </strong> :  {{$row->mobile}}
<p>   
<p>
    <strong>{{trans("app.Email")}} </strong> :  {{$row->email}}
<p>
<p>
    <strong>{{trans("app.Content")}} </strong> :  {!! $row->content !!}
<p>
    @endsection
