@extends('layouts.auth')
@section('content')
<div class="signin-box">
    <h3 class="signin-title-secondary">{{$page_title}}</h3>
    {!! Form::model($row,['method' => 'post','files' => true] ) !!}
    {{ csrf_field() }}
    @php
    $attributes=['class'=>'form-control','label'=>trans('app.Name'),'placeholder'=>trans('app.Name'),'required'=>1];
    @endphp
    @include('form.input',['name'=>'name','type'=>'text','attributes'=>$attributes])

    @php
    $attributes=['class'=>'form-control','label'=>trans('app.Email'),'placeholder'=>trans('app.Email'),'required'=>1];
    @endphp
    @include('form.input',['name'=>'email','type'=>'email','attributes'=>$attributes])

    @php
    $attributes=['class'=>'form-control','label'=>trans('app.Mobile'),'placeholder'=>trans('app.Mobile'),'required'=>1];
    @endphp
    @include('form.input',['name'=>'mobile','type'=>'text','attributes'=>$attributes])

    @include('form.input',['name'=>'date_of_birth','type'=>'text','attributes'=>['class'=>'form-control datepicker','label'=>trans('app.Date of Birth'),'placeholder'=>trans('app.Date of Birth'),'required'=>1,'autocomplete'=>'off']])

    @php
    $options=languages();
    $attributes=['class'=>'form-control','label'=>trans('app.Default language'),'placeholder'=>trans('app.Default language'),'required'=>1];
    @endphp
    @include('form.select',['name'=>'language','options'=>$options,'attributes'=>$attributes])

    @include('form.input',['name'=>'bio','type'=>'textarea','attributes'=>['class'=>'form-control editor','label'=>trans('app.Bio'),'placeholder'=>trans('app.Bio')]])

    @include('form.select',['name'=>'languages_spoken[]','options'=>$row->getLanguages(),'attributes'=>['class'=>'form-control select2','label'=>trans('app.Language spoken'),'multiple'=>true]])

    @include('form.select',['name'=>'transportation[]','options'=>$row->getTransportations(),'attributes'=>['class'=>'form-control select2','label'=>trans('app.Transportation'),'multiple'=>true]])

    @include('form.input',['name'=>'qualifications','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('app.Qualifications'),'placeholder'=>trans('app.Qualifications')]])

    @include('form.input',['name'=>'work_experience','type'=>'textarea','attributes'=>['class'=>'form-control','label'=>trans('app.Work experience'),'placeholder'=>trans('app.Work experience')]])

    @include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('app.Image'),'placeholder'=>trans('app.Image')]])

    <h4>{{trans('users.Address information')}}</h4>

    @include('form.select',['name'=>'country_id','options'=>$row->getCountries(),'attributes'=>['id'=>'country_code','class'=>'form-control','label'=>trans('users.Country'),'placeholder'=>trans('users.Country')]])

    @include('form.input',['name'=>'city','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.City'),'placeholder'=>trans('users.City')]])

    @include('form.input',['name'=>'street_name','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Street name'),'placeholder'=>trans('users.Street name')]])

    @include('form.input',['name'=>'house_number','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.House number'),'placeholder'=>trans('users.House number')]])

    @include('form.input',['name'=>'postal_code','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Postal code'),'placeholder'=>trans('users.Postal code')]])



    <h4>{{trans('app.Bank information')}}</h4>
    @include('form.input',['name'=>'bank_name','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('app.Bank name'),'placeholder'=>trans('app.Bank name')]])

    @include('form.input',['name'=>'bank_account_number','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('app.Bank account number'),'placeholder'=>trans('app.Bank account number')]])

    @include('form.input',['name'=>'swift_code','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('app.Swift code'),'placeholder'=>trans('app.Swift code')]])

    <!-- custom-file -->
    <div class="form-layout-footer mg-t-30">
        <button class="btn btn-primary bd-0">{{ trans('app.Save') }}</button>
    </div>
    {!! Form::close() !!}
</div>
@endsection
