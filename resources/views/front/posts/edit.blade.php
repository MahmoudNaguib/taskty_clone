
@extends('front.layouts.master')

@section('content')
<div class="course-area section-padding course-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{$page_title}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-wrapper mb-20">
            <div class="form-layout form-layout-4">
                @include('front.partials.breadcrumb')
                {!! Form::model($row,['method' => 'post','files' => true] ) !!} {{ csrf_field() }}
                @include('front.'.$module.'.form',$row)
                <!-- custom-file -->
                <div class="form-layout-footer mg-t-30">
                    <button class="btn btn-primary bd-0">{{ trans('app.Save') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- form-layout -->
        </div>
    </div>
</div>
@endsection

