@extends('front.layouts.master')

@section('content')
<div class="course-area section-padding course-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{$page_title}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-wrapper mb-20">
            @include('front.partials.breadcrumb')
            <div class="form-layout form-layout-4">
                <a href="{{$module}}/edit/{{$row->id}}" class="btn btn-success">
                    <i class="fa fa-edit"></i> {{trans('app.Edit')}}
                </a><br>
                <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">

                        @foreach(langs() as $lang)
                        <tr>
                            <td width="25%" class="align-left">{{trans('app.Title')}} ({{$lang}})</td>
                            <td width="75%" class="align-left">{{@$row->getTranslation('title',$lang)}}</td>
                        </tr>
                        @endforeach

                        @foreach(langs() as $lang)
                        <tr>
                            <td width="25%" class="align-left">{{trans('app.Slug')}} ({{$lang}})</td>
                            <td width="75%" class="align-left">{{@$row->getTranslation('slug',$lang)}}</td>
                        </tr>
                        @endforeach

                        @foreach(langs() as $lang)
                        <tr>
                            <td width="25%" class="align-left">{{trans('app.Content')}} ({{$lang}})</td>
                            <td width="75%" class="align-left">{!! @$row->getTranslation('content',$lang) !!}</td>
                        </tr>
                        @endforeach

                        @foreach(langs() as $lang)
                        <tr>
                            <td width="25%" class="align-left">{{trans('app.Tags')}} ({{$lang}})</td>
                            <td width="75%" class="align-left">{{@$row->getTranslation('tags',$lang)}}</td>
                        </tr>
                        @endforeach

                        @foreach(langs() as $lang)
                        <tr>
                            <td width="25%" class="align-left">{{trans('app.Meta description')}} ({{$lang}})</td>
                            <td width="75%" class="align-left">{{@$row->getTranslation('meta_description',$lang)}}</td>
                        </tr>
                        @endforeach

                        @foreach(langs() as $lang)
                        <tr>
                            <td width="25%" class="align-left">{{trans('app.Meta keywords')}} ({{$lang}})</td>
                            <td width="75%" class="align-left">{{@$row->getTranslation('meta_keywords',$lang)}}</td>
                        </tr>
                        @endforeach

                        <tr>
                            <td width="25%" class="align-left">{{trans('app.Image')}}</td>
                            <td width="75%" class="align-left">{!! image($row->image,'small') !!}</td>
                        </tr>
                        <tr>
                            <td width="25%" class="align-left">{{trans('app.Is active')}}</td>
                            <td width="75%" class="align-left"><img src="backend/img/{{($row->is_active)?'check.png':'close.png'}}"></td>
                        </tr>
                        <tr>
                            <td width="25%" class="align-left">{{trans('app.Created by')}}</td>
                            <td width="75%" class="align-left">{{@$row->creator->name}}</td>
                        </tr>

                    </table>
                </div>
            </div>
            <!-- form-layout -->
        </div>
    </div>
</div>
@endsection
