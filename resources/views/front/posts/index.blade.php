@extends('front.layouts.master')

@section('content')
<div class="course-area section-padding course-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{$page_title}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-wrapper mb-20">
            @include('front.partials.breadcrumb')
            <div>
                <a href="{{$module}}/create" class="btn btn-success">
                    <i class="fa fa-plus"></i> {{trans('app.Create')}}
                </a>
            </div>
            @if (!$rows->isEmpty())
            <div class="table-responsive">
                <table class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-5p">{{trans('app.ID')}} </th>
                            <th class="wd-30p">{{trans('app.Title')}} </th>
                            <th class="wd-10p">{{trans('app.Views count')}} </th>
                            <th class="wd-5p">{{trans('app.Active')}} </th>
                            <th class="wd-10p">{{trans('app.Created by')}} </th>
                            <th class="wd-15p">{{trans('app.Created at')}}</th>
                            <th class="wd-20p">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($rows as $row)
                        <tr>
                            <td class="center">{{$row->id}}</td>
                            <td class="center">{{str_limit($row->title,50)}}</td>
                            <td class="center">{{$row->views}}</td>
                            <td class="center"><img src="backend/img/{{($row->is_active)?'check.png':'close.png'}}"></td>
                            <td class="center">{{@$row->creator->name}}</td>
                            <td class="center">{{$row->created_at}}</td>
                            <td class="center">
                                <a class="btn btn-success btn-xs" href="{{$module}}/edit/{{$row->id}}" title="{{trans('app.Edit')}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a class="btn btn-primary btn-xs" href="{{$module}}/view/{{$row->id}}" title="{{trans('app.View')}}">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a class="btn btn-danger btn-xs" href="{{$module}}/delete/{{$row->id}}" title="{{trans('app.Delete')}}" data-confirm="{{trans('app.Are you sure you want to delete this item')}}?">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="paganition-center"> 
                {!! $rows->appends([])->render() !!}
            </div>
            @else
            {{trans("posts.There is no results")}}
            @endif
            <!-- form-layout -->
        </div>
    </div>
</div>
@endsection


