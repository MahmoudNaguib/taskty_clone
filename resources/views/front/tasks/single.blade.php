<div class="col-lg-4 col-md-6 col-12">
    <div class="single-item">
        <div class="single-item-text">
            <h4><a href="{{$row->link}}">{{ucfirst($row->title)}}</a></h4>
            <div class="single-item-text-info">
                <span>{{trans('app.Section')}}: <span>
                        <a href="{{lang()}}/tasks?section_id={{$row->section_id}}">{{$row->section->title}}</a>
                    </span>
                </span>
            </div>
            <div class="single-item-text-info">
                <span>{{trans('app.By')}}: <span>{{$row->creator->name}}</span></span>
                <span>{{trans('app.Status')}}: <span>{{$row->status_value}}</span></span>
                <span>{{trans('app.Views')}}: <span>{{$row->views}}</span></span>
            </div>
            <div class="single-item-text-info">
                <span>{{trans('app.Date')}}: <span>{{$row->date}}, {{@$row->time_value}}</span></span>
            </div>

            <div class="single-item-text-info">
                <span>{{trans('app.Where')}}: <span>{{$row->where_value}}</span></span>
                <span>{{trans('app.Country')}}: <span>{{$row->country->title}}</span></span>
            </div>
            <div class="single-item-text-info">
                <span>{{trans('app.Location')}}: <a href='https://www.google.com/maps/place/{{$row->latitude}},{{@$row->longitude}}' target="_blank" class="text-success">{{$row->location}}</a>
                </span>
            </div>

            <p>{{$row->content_limited}}</p>
            <p class="text-center">
                <span class="btn btn-success">{{$row->budget}} <span class="currency">{{trans('app.EUR')}}</span></span>
            </p>
            @include('front.tasks.partials.counts')
        </div>
        <div class="button-bottom">
            <a href="{{$row->link}}" class="btn btn-theme btn-secondary">{{trans('app.View')}}</a>
<!--            @if($row->created_by==@auth()->user()->id && ($row->status=='draft' || $row->offers->count()==0))
            <a href="{{lang()}}/tasks/edit/{{$row->id}}" class="btn btn-theme btn-success">{{trans('app.Edit')}}</a>
            @endif
            @if($row->created_by==@auth()->user()->id)
            <a href="{{lang()}}/tasks/delete/{{$row->id}}" class="btn btn-theme btn-danger" data-confirm="{{trans('app.Are you sure you want to delete this item')}}?">{{trans('app.Delete')}}</a>
            @endif-->
        </div>
    </div>
</div>
