@include('form.select',['name'=>'section_id','options'=>$row->getSectionsWithParents(),'attributes'=>['class'=>'form-control','label'=>trans('app.Section'),'placeholder'=>trans('app.Select Section'),'required'=>1]])

@include('form.input',['name'=>'title','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('app.Title'),'placeholder'=>trans('app.Title'),'required'=>1]])

@include('form.input',['name'=>'content','type'=>'textarea','attributes'=>['class'=>'form-control editor','label'=>trans('app.Content'),'placeholder'=>trans('app.Content'),'required'=>1]])

@include('form.select',['name'=>'where','options'=>$row->getWhereValues(),'attributes'=>['class'=>'form-control','label'=>trans('app.Where'),'placeholder'=>trans('app.Where'),'required'=>1]])

@include('form.select',['name'=>'country_id','options'=>$row->getCountries(),'attributes'=>['class'=>'form-control','label'=>trans('app.Country'),'placeholder'=>trans('app.Country'),'required'=>1]])

@include('form.input',['name'=>'location','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('app.Location'),'placeholder'=>trans('app.Location'),'required'=>1,'id'=>'location']])
<div id = "map" style = "width:100%; height:400px;"> </div>
<input type="hidden" name='latitude' id="latitude" value="{{(old('latitude'))?:($row->latitude)?:'30.044355716930493'}}">
<input type="hidden" name='longitude' id="longitude" value="{{(old('longitude'))?:($row->longitude)?:'31.235783100128174'}}">

<br>
@include('form.input',['name'=>'date','type'=>'text','attributes'=>['class'=>'form-control datepicker','label'=>trans('app.Date'),'placeholder'=>trans('app.Date'),'required'=>1]])

@include('form.select',['name'=>'time','options'=>$row->getTimeValues(),'attributes'=>['class'=>'form-control','label'=>trans('app.Time'),'placeholder'=>trans('app.Time'),'required'=>1]])

@include('form.input',['name'=>'budget','type'=>'number','attributes'=>['class'=>'form-control','label'=>trans('app.Budget'),'placeholder'=>trans('app.Budget'),'step'=>'0.01','required'=>1,'min'=>0,'pattern'=>'^\d*(\.\d{0,2})?$']])

@include('form.select',['name'=>'status','options'=>$row->getStatusValues(),'attributes'=>['class'=>'form-control','label'=>trans('app.Status'),'placeholder'=>trans('app.Status'),'required'=>1]])

@include('form.input',['name'=>'tags','type'=>'text','attributes'=>['class'=>'form-control tags','label'=>trans('app.Tags'),'placeholder'=>trans('app.Tags')]])

@include('form.file',['name'=>'attachment','attributes'=>['file_type'=>'attachment','class'=>'form-control attach custom-file-input','label'=>trans('app.Attachment'),'placeholder'=>trans('app.Attachment')]])


@push('js')
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key={{env('GOOGLE_MAP_API_KEY2')}}&libraries=places"></script>

<script type="text/javascript">
var map;
var lat = $("#latitude").val();
var lng = $("#longitude").val();
function intialize(lat, lng) {
    console.log("intialize new map");
    console.log(lat,lng);
    //$(".readonly").attr("readonly", "TRUE");
    var latlng = new google.maps.LatLng(lat, lng);
    var options = {
        zoom: 15,
        center: latlng,
        //scrollwheel: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), options);
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        draggable: true,
    });
    google.maps.event.addListener(marker, 'dragend', function (evt) {
        console.log(evt.latLng.lat());
        console.log(evt.latLng.lng());
        $("#latitude").val(evt.latLng.lat());
        $("#longitude").val(evt.latLng.lng());
        map.setCenter(marker.getPosition());
    });
    var input = document.getElementById('location');
    var options = {
//        types: ['(cities)'],
//        componentRestrictions: {country: "eg"}
    };
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    autocomplete.addListener('place_changed', function () {
        console.log(autocomplete.getPlace());
        console.log(autocomplete.getPlace().geometry.location.lat());
        $("#latitude").val(autocomplete.getPlace().geometry.location.lat());
        $("#longitude").val(autocomplete.getPlace().geometry.location.lng());
        intialize(autocomplete.getPlace().geometry.location.lat(), autocomplete.getPlace().geometry.location.lng());
    });

}
$(function () {
    console.log("lat: " + lat);
    intialize(lat, lng);
});
$(window).resize(function () {
    lat = $("#latitude").val();
    lng = $("#longitude").val();
    if (lat != "" || lat != undefined) {
        if (lng != "" || lng != undefined) {
            intialize(lat, lng);
        }
    }
});
</script>
@endpush                
<style>
    #map img {
        max-width:none;
    }
</style>
