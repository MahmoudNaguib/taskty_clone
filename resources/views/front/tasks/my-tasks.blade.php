@extends('front.layouts.master')

@section('content')
    <div class="course-area section-padding course-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title-wrapper">
                        <div class="section-title">
                            <h3>
                                {{$page_title}}
                            </h3>
                            <p></p>
                        </div>
                    </div>
                </div>
                @if(@auth()->user()->is_tasker)
                    <a href="{{lang()}}/tasks/create" class="btn btn-success">
                        {{trans('app.Create task')}}
                    </a>
                @endif
                @include('front.'.$module.'.partials.filter')
                <div class="row">
                    <div class="container mt-40 mb-40">
                        @if (!$rows->isEmpty())
                            <div class="table-responsive">
                                <table class="table display responsive nowrap dataTable">
                                    <thead>
                                    <tr>
                                        <th class="wd-5p">{{trans('tasks.ID')}} </th>
                                        <th class="wd-10p">{{trans('tasks.Section')}} </th>
                                        <th class="wd-10p">{{trans('tasks.Title')}} </th>
                                        <th class="wd-5p">{{trans('tasks.Where')}} </th>
                                        <th class="wd-5p">{{trans('tasks.Date')}} </th>
                                        <th class="wd-5p">{{trans('tasks.Time')}} </th>
                                        <th class="wd-5p">{{trans('tasks.Status')}} </th>
                                        <th class="wd-10p">{{trans('tasks.Created at')}}</th>
                                        <th class="wd-20p">&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($rows as $row)
                                        <tr>
                                            <td class="center">{{$row->id}}</td>
                                            <td class="center">{{$row->section->title}}</td>
                                            <td class="center">{{str_limit($row->title,50)}}</td>
                                            <td class="center">{{$row->where_value}}</td>
                                            <td class="center">{{$row->date}}</td>
                                            <td class="center">{{$row->time_value}}</td>
                                            <td class="center">
                                                {{$row->status_value}}
                                                @if($row->status!='closed' && $row->status!='done')
                                                    <a class="btn btn-sm btn-primary" href="{{$module}}/close/{{$row->id}}"
                                                       title="{{trans('posts.Close task')}}">
                                                        {{trans('app.Close')}}
                                                    </a>
                                                @endif


                                            </td>
                                            <td class="center">{{str_limit($row->created_at,10,false)}}</td>
                                            <td class="center">
                                                @if($row->created_by==@auth()->user()->id && ($row->status=='draft' || $row->offers->count()==0))
                                                    <a class="btn btn-sm btn-success" href="{{$module}}/edit/{{$row->id}}"
                                                       title="{{trans('posts.Edit')}}">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endif

                                                <a class="btn btn-sm btn-primary" href="{{$module}}/details/{{$row->id}}"
                                                   title="{{trans('tasks.View')}}">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                @if($row->created_by==@auth()->user()->id)
                                                    <a class="btn btn-sm btn-danger" href="{{$module}}/delete/{{$row->id}}"
                                                       title="{{trans('tasks.Delete')}}"
                                                       data-confirm="{{trans('tasks.Are you sure you want to delete this item')}}?">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                @endif
                                                @if($row->status=='done')
                                                    <a class="btn btn-sm btn-primary" href="{{$module}}/rate/{{$row->id}}" title="{{trans('tasks.Rate and Review')}}">
                                                        <i class="fa fa-star"></i>
                                                        {{trans('app.Rate')}}
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{trans("app.There is no results")}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
