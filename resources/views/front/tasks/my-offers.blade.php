@extends('front.layouts.master')

@section('content')
<div class="container">
    <div class="course-area section-padding course-page">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>
                            {{$page_title}}
                        </h3>
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-40 mb-40">
                @if (!$rows->isEmpty())
                <div class="table-responsive">
                    <table class="table display responsive nowrap dataTable">
                        <thead>
                            <tr>
                                <th class="wd-5p">{{trans('app.ID')}} </th>
                                <th class="wd-20p">{{trans('app.Task')}} </th>
                                <th class="wd-15p">{{trans('app.Budget')}} </th>
                                <th class="wd-10p">{{trans('app.Attachment')}} </th>
                                <th class="wd-10p">{{trans('app.Is accepted')}} </th>
                                <th class="wd-10p">{{trans('app.Is paid')}} </th>
                                <th class="wd-10p">{{trans('app.Created at')}}</th>
                                <th class="wd-20p">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rows as $row)
                            <tr>
                                <td class="center">{{$row->id}}</td>
                                <td class="center">
                                    <a href="{{$row->task->link}}" target="_blank">
                                        {{$row->task->title}}
                                    </a>
                                </td>
                                <td class="center">{{$row->budget}} <span class="currency">{{trans('app.EUR')}}</span></td>
                                <td class="center">
                                    {!! fileRender($row->attachment) !!}
                                </td>
                                <td class="center">{{($row->is_accepted)?trans('app.Yes'):trans('app.No')}}</td>
                                <td class="center">{{($row->is_paid)?trans('app.Yes'):trans('app.No')}}</td>
                                <td class="center">{{str_limit($row->created_at,10,false)}}</td>
                                <td class="center">
                                    <a href="tasks/view-offer/{{$row->id}}" title="{{trans('app.View offer')}}" data-toggle="modal" data-target="#view-offer-{{$row->id}}" class="btn btn-primary btn-xs">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <div class="modal fade" id="view-offer-{{$row->id}}" role="document">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content bd-0">
                                                <div class="modal-header pd-y-20 pd-x-25">
                                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{trans('app.Offer')}}</h6>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body pd-25">
                                                    @include('front.tasks.partials.offer',['record'=>$row])
                                                </div>
                                            </div>
                                        </div><!-- modal-dialog -->
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                {{trans("app.There is no results")}}
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
