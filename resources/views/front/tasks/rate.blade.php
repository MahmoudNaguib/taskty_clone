@extends('front.layouts.master')

@section('content')
<div class="course-area section-padding course-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{$page_title}}</h3>
                        <p></p>
                    </div>
                    <a href="{{lang()}}/tasks/my-tasks" class="btn btn-primary">
                        {{trans('app.Back to my tasks')}}
                    </a>
                </div>

            </div>
        </div>
        <div class="section-wrapper">

            <div class="form-layout form-layout-4">
                {!! Form::model($rate,['url'=>lang().'/tasks/rate/'.$row->id,'method' => 'post','files' => true] ) !!}
                {{ csrf_field() }}
                @include('form.select',['name'=>'value','options'=>[1,2,3,4,5],'attributes'=>['class'=>'form-control','label'=>trans('app.Rate'),'placeholder'=>trans('app.Rate'),'required'=>1]])
                @include('form.input',['name'=>'review','type'=>'textarea','attributes'=>['class'=>'form-control editor','label'=>trans('app.Review'),'placeholder'=>trans('app.Review'),'required'=>1]])
                <!-- custom-file -->
                <div class="form-layout-footer mg-t-30 mg-b-30">
                    <button class="button-default">{{ trans('app.Save') }}</button>
                </div>
                {!! Form::close() !!}
                <br><br>
                <!-- form-layout-footer -->
            </div>
            <!-- form-layout -->
        </div>
    </div>
</div>
@endsection

