@extends('front.layouts.master')

@section('content')
<div class="course-area section-padding course-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>
                            {{$page_title}}

                        </h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        @include('front.'.$module.'.partials.filter')
        <div class="row mt-40 mb-40">
            @if (!$rows->isEmpty())
            @foreach ($rows as $row)
            @include('front.tasks.single')
            @endforeach
            <div class="container">
                <div class="paganition-center center">
                    {!! $rows->appends(['section_id'=>request('section_id'),'where'=>request('where'),'country_id'=>request('country_id'),'from_date'=>request('from_date'),'to_date'=>request('to_date'),'time'=>request('time'),'budget'=>request('budget')])->render() !!}
                </div>
            </div>
            @else
            {{trans("app.There is no results")}}
            @endif
        </div>
    </div>
</div>
@endsection
