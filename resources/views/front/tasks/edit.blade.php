@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    <div class="form-layout form-layout-4">
        {!! Form::model($row,['method' => 'post','files' => true] ) !!} {{ csrf_field() }}
        @include($module.'.form',$row)
        <!-- custom-file -->
        <div class="form-layout-footer mg-t-30">
            <button class="button-default">{{ trans('app.Save') }}</button>
        </div>
        {!! Form::close() !!}
        <!-- form-layout-footer -->
        <br><br>
    </div>
    <!-- form-layout -->
</div>
@endsection
