{!! Form::model($row,['method' => 'get','files' => true] ) !!} 
<div class="row">
    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        {!! Form::select('section_id',  $row->getSectionsWithParents(),@request('section_id'), ['class'=>'form-control','placeholder'=>trans('app.Section')]) !!}
    </div><!-- col-4 -->
    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        {!! Form::select('where',  $row->getWhereValues(),@request('where'), ['class'=>'form-control','placeholder'=>trans('app.Where')]) !!}
    </div><!-- col-4 -->
    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        {!! Form::select('country_id',  $row->getCountries(),@request('country_id'), ['class'=>'form-control','placeholder'=>trans('app.Country')]) !!}
    </div><!-- col-4 -->

    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        {!! Form::text('from_date',@request('from_date'),['class'=>'form-control datepicker','placeholder'=>trans('app.From'),'autocomplete'=>'off']) !!}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        {!! Form::text('to_date',@request('to_date'),['class'=>'form-control datepicker','placeholder'=>trans('app.To'),'autocomplete'=>'off']) !!}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        {!! Form::select('time',  $row->getTimeValues(),@request('time'), ['class'=>'form-control','placeholder'=>trans('app.Time')]) !!}
    </div><!-- col-4 -->
    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        {!! Form::number('budget',@request('budget'),['class'=>'form-control','placeholder'=>trans('app.Budget').' '.trans('app.Less than')]) !!}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        <button class="btn btn-theme btn-secondary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Filter') }}</button> 
        <a href="{{$module}}" class="btn btn-theme btn-secondary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Reset') }}</a>
    </div>
</div>
{!! Form::close() !!}