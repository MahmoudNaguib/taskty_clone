<div class="single-item-content">
    <div class="single-item-comment-view">
        <span><i class="zmdi zmdi-eye"></i>{{$row->views}} {{trans('app.Views')}}</span>
        <span><i class="zmdi zmdi-comments"></i>{{$row->questions->count()}} {{trans('app.Comments')}}</span>
        <span><i class="zmdi zmdi-wrench"></i>{{$row->offers->count()}} {{trans('app.Offers')}}</span>
    </div>
    @if($row->rate)
    <div class="single-item-rating">
        @for($i=1; $i<=$row->rate; $i++)
        <i class="zmdi zmdi-star"></i>
        @endfor
        @for($i=1; $i<=5-$row->rate; $i++)
        <i class="zmdi zmdi-star-outline"></i>
        @endfor
    </div>
    @endif
</div>  
