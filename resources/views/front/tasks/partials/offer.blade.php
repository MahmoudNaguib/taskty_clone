<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
        <tr>
            <td width="25%" class="align-left">{{trans('app.Task')}}</td>
            <td width="75%" class="align-left">{{$row->task->title}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('app.Content')}}</td>
            <td width="75%" class="align-left">{!! wordwrap(@$record->content,75,'<br>',true) !!}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('app.Budget')}}</td>
            <td width="75%" class="align-left">{{$record->budget}} {{trans('app.EUR')}}</td>
        </tr>
        @if($record->attachment)
            <tr>
                <td width="25%" class="align-left">{{trans('app.Attachment')}}</td>
                <td width="75%" class="align-left">{!! fileRender($record->attachment) !!}</td>
            </tr>
        @endif
        <tr>
            <td width="25%" class="align-left">{{trans('app.Created by')}}</td>
            <td width="75%" class="align-left">{{$record->creator->name}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('app.Is accepted')}}</td>
            <td width="75%" class="align-left">{{($record->is_accepted)?trans('app.Yes'):trans('app.No')}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('app.Is paid')}}</td>
            <td width="75%" class="align-left">{{($record->is_paid)?trans('app.Yes'):trans('app.No')}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('app.Created at')}}</td>
            <td width="75%" class="align-left">{{$record->created_at}}</td>
        </tr>

    </table>
</div>
