@extends('front.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="section-title-wrapper">
                <div class="section-title mt-25">
                    <h3>{{$page_title}}</h3>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
    <div class="course-details-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-12 col-12">
                    <div class="single-course-details">
                        <div class="col-md-12">
                            <div class="single-item-text">
                                <div class="single-item-text-info">
                                    <span>{{trans('app.Section')}}:
                                        <span>
                                            <a
                                                href="{{lang()}}/tasks?section_id={{$row->section_id}}">{{$row->section->title}}</a>
                                        </span>
                                    </span>
                                </div>
                                <div class="single-item-text-info">
                                    <span>{{trans('app.By')}}: <span>{{$row->creator->name}}</span></span>
                                    <span>{{trans('app.Status')}}: <span>{{$row->status_value}}</span></span>
                                    <span>{{trans('app.Views')}}: <span>{{$row->views}}</span></span>
                                </div>
                                <div class="single-item-text-info">
                                    <span>{{trans('app.Date')}}: <span>{{$row->date}}, {{@$row->time_value}}</span></span>
                                </div>
                                <div class="single-item-text-info">
                                    <span>{{trans('app.Where')}}: <span>{{$row->where_value}}</span></span>
                                    <span>{{trans('app.Country')}}: <span>{{$row->country->title}}</span></span>
                                </div>
                                <div class="single-item-text-info">
                                        <span>{{trans('app.Location')}}: <a
                                                href='https://www.google.com/maps/place/{{$row->latitude}},{{@$row->longitude}}'
                                                target="_blank">{{$row->location}}</a>
                                        </span>
                                </div>

                                @if($row->attachment)
                                    <div class="single-item-text-info">
                                        <p>{{trans('app.Attachment')}}:
                                        <p>
                                        {!! filePreview($row->attachment) !!}
                                    </div>
                                @endif
                                <div class="course-text-content">
                                    <p>{!! $row->content !!}</p>
                                    <div class="row news-details-content ">
                                        @include('front.tasks.partials.share')
                                    </div>
                                </div>
                                @include('front.tasks.partials.counts')
                                @if($row->tags)
                                    <div class="comments">
                                        <h4 class="title">{{trans('app.Tags')}}</h4>
                                        @foreach(explode(',',$row->tags) as $tag)
                                            <a href="{{lang()}}/tasks?tags={{$tag}}">{{$tag}}</a>,
                                        @endforeach
                                    </div>
                                @endif


                            </div>
                        </div>
                    </div>
                    @if(auth()->user())
                        <div class="comments contact-form-area">
                            @if(auth()->user())
                                <h4 class="title">{{trans('app.Write your question')}}/{{trans('app.comment')}}</h4>
                                <form id="contact-form" action="{{lang()}}/questions/create?task_id={{$row->id}}" method="post"
                                      accept-charset="UTF-8" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                                        <textarea name="content" cols="30" rows="10"
                                                                  placeholder="{{trans('app.Content')}}"></textarea>
                                            @if(@$errors)
                                                @foreach($errors->get('content') as $message)
                                                    <span class='help-inline text-danger'>{{ $message }}</span>
                                                @endforeach
                                            @endif

                                            <input type="file" name="attachment" class="form-control"
                                                   placeholder="{{trans('app.Attachment')}}">
                                            @if(@$errors)
                                                @foreach($errors->get('attachment') as $message)
                                                    <span class='help-inline text-danger'>{{ $message }}</span>
                                                @endforeach
                                            @endif

                                            <br>
                                            <button type="submit" class="button-default">{{trans('app.SUBMIT')}}</button>
                                        </div>
                                    </div>
                                </form>
                            @else
                                <h4 class="title">{{trans('app.Need to write your question')}}/{{trans('app.comment')}} <a
                                        href="{{lang()}}/auth/login">{{trans('app.Login')}}</a></h4>
                            @endif
                        </div>
                        @php
                            $questions=$row->questions()->latest()->get();
                        @endphp

                        <div class="comments">
                            <h4 class="title">{{trans('app.Questions')}}/{{trans('app.Comments')}}</h4>
                            @if(!$questions->isEmpty())
                                @foreach($questions as $question)
                                    <div class="single-comment">
                                        <div class="author-image">
                                            <a href="{{@$question->creator->link}}">
                                                <img src="uploads/small/{{$question->creator->image}}"
                                                     alt="{{@$question->creator->name}}-{{$question->id}}">
                                            </a>
                                        </div>
                                        <div class="comment-text">
                                            <div class="author-info">
                                                <h4><a href="#">{{$question->creator->name}}</a></h4>
                                                <span class="comment-time">{{trans('app.Posted on')}} {{$question->created_at}}</span>
                                            </div>
                                            <p>{{$question->content}}</p>
                                            @if($question->attachment)
                                                <p>{{trans('app.Attachments')}}: {!! fileRender($question->attachment) !!}</p>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                {{trans('app.There is no questions')}}
                            @endif
                        </div>
                    @endif
                </div>
                <div class="col-lg-3 col-md-12 col-12">
                    <div class="single-sidebar-widget">
                        <div class="tution-wrapper">
                            <div class="tution-fee">
                                <h3>{{trans('app.Budget')}}</h3>
                                <h1>{{$row->budget}} <span class="currency">{{trans('app.EUR')}}</span></h1>
                            </div>
                            <div class="tutor-image">
                                <img src="uploads/small/{{$row->creator->image}}" alt="{{$row->creator->name}}">
                            </div>
                            <div class="single-teacher-text">
                                <h3><a href="{{$row->creator->link}}">{{$row->creator->name}}</a></h3>
                                <p>{{$row->creator->bio_limited}}</p>
                                <div class="single-item-comment-view">
                                    <span>{{$row->creator->tasks()->count()}} {{trans('app.Tasks')}}</span></div>
                                @if($rate=$row->creator->rate)
                                    <div class="single-item-rating">
                                        @for($i=1; $i<=$rate; $i++)
                                            <i class="zmdi zmdi-star"></i>
                                        @endfor
                                        @for($i=1; $i<=5-$rate; $i++)
                                            <i class="zmdi zmdi-star-outline"></i>
                                        @endfor
                                    </div>
                                @endif
                            </div>
                        </div>
                        @if(auth()->user())
                            @if(!in_array($row->id,auth()->user()->applied) && !in_array($row->id,auth()->user()->own) && $row->status=='open')
                                <div class="tution-wrapper">
                                    <div class="comments contact-form-area">
                                        <h4 class="title">{{trans('app.Make offer')}}</h4>
                                        <form id="contact-form" action="{{lang()}}/offers/create?task_id={{$row->id}}" method="post"
                                              accept-charset="UTF-8" enctype="multipart/form-data">
                                            <div>
                                                    <textarea name="content" placeholder="{{trans('app.Content')}}"
                                                              required="1">{{old('content')}}</textarea>
                                                @if(@$errors)
                                                    @foreach($errors->get('content') as $message)
                                                        <span class='help-inline text-danger'>{{ $message }}</span>
                                                    @endforeach
                                                @endif

                                                <input class="form-control" placeholder="{{trans('app.Budget')}} ({{trans('app.EUR')}})"
                                                       step="0.01" min="0" pattern="^\d*(\.\d{0,2})?$" autocomplete="off" name="budget"
                                                       type="number" value="{{old('budget')}}" required="1">
                                                @if(@$errors)
                                                    @foreach($errors->get('budget') as $message)
                                                        <span class='help-inline text-danger'>{{ $message }}</span>
                                                    @endforeach
                                                @endif

                                                <input type="file" name="attachment" class="form-control"
                                                       placeholder="{{trans('app.Attachment')}}">
                                                @if(@$errors)
                                                    @foreach($errors->get('attachment') as $message)
                                                        <span class='help-inline text-danger'>{{ $message }}</span>
                                                    @endforeach
                                                @endif

                                                <br>
                                                <button type="submit" class="button-default">{{trans('app.SUBMIT')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @else
                                <div class="tution-wrapper">
                                    @php
                                        $offer=\App\Models\Offer::where('task_id',$row->id)->where('created_by',auth()->user()->id)->first();
                                    @endphp
                                    @if($offer)
                                        <div class="row">
                                            <div class="col-md-12 {{($row->status=='assigned' && $row->offer_id==$offer->id)?'accepted':''}}">
                                                <h4 class="title">{{trans('app.Your offer')}}

                                                    @if($row->status=='open')
                                                        <a href="{{lang()}}/offers/delete/{{$offer->id}}" class="btn btn-danger"
                                                           data-confirm="{{trans('app.Are you sure you want to delete this item')}}?">
                                                            {{trans('app.Delete')}}
                                                        </a>
                                                    @endif
                                                </h4>
                                                <div class="single-item-text-info">
                                            <span>{{trans('app.Content')}}: {{$offer->content}}
                                            </span>
                                                </div>
                                                <div class="single-item-text-info">
                                            <span>{{trans('app.Budget')}}: {{$offer->budget}} <span class="currency">{{trans('app.EUR')}}</span>
                                            </span>
                                                </div>
                                                @if($offer->attachment)
                                                    <div class="single-item-text-info">
                                                        <span>{{trans('app.Attachments')}}: {!! fileRender($offer->attachment) !!}  </span>
                                                    </div>
                                                @endif
                                                @if($row->status=='assigned' && $row->offer_id==$offer->id)
                                                    <div class="single-item-text-info text-success">
                                                        <span>{{trans('app.Task has been assigned to you')}}</span>
                                                        <a class="btn btn-success btn-sm"
                                                           href="{{lang()}}/tasks/done/{{$row->id}}">{{trans('app.Make it done')}}
                                                            / {{trans('app.Request payment')}}</a>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                            @if(in_array($row->id,auth()->user()->own))
                                <div class="offers">
                                    @php
                                        $offers=$row->offers()->latest()->get();
                                    @endphp
                                    <h4 class="title">{{trans('app.Offers')}}</h4>
                                    @if(!$offers->isEmpty())
                                        @foreach($offers as $offer)
                                            <div class="container {{($offer->is_accepted)?'accepted':''}}">
                                                <h5 class="title">
                                                    <a href="{{$offer->creator->link}}">{{$offer->creator->name}}</a>
                                                </h5>

                                                <p>{{$offer->content}}</p>
                                                <p>
                                                    {{trans('app.Budget')}}: <span class="badge badge-success">{{$offer->budget}} <span class="currency">{{trans('app.EUR')}}</span></span>
                                                </p>
                                                @if($offer->attachment)
                                                    <p>{{trans('app.Attachments')}}: {!! fileRender($offer->attachment) !!}</p>
                                                @endif
                                                <p>
                                                    {{trans('app.Posted on')}}: {{$offer->created_at}}

                                                </p>
                                                <p>
                                                    @if(!$row->offer_id)
                                                        <a href="{{lang()}}/offers/select/{{$offer->id}}" class="btn btn-success">
                                                            {{trans('app.Select offer')}}</a>
                                                    @endif
                                                </p>

                                                <p>
                                                    {{trans('app.Task status')}}:
                                                    <span class="badge badge-success">{{$offer->task->status_value}}</span>
                                                </p>
                                                @if($offer->is_paid==0 && $offer->is_accepted &&  ($row->status=='assigned' || $row->status=='done'))
                                                <p>
                                                    <a class="btn btn-success btn-sm"
                                                           href="{{lang()}}/transactions/transfer/{{$offer->id}}">{{trans('app.Transfer payment')}}</a>
                                                    </p>
                                              @endif
                                            </div>
                                            <hr>
                                        @endforeach
                                    @else
                                        {{trans('app.There is no offers')}}
                                    @endif
                                </div>
                            @endif
                        @else
                            <h4 class="title">{{trans('app.Need to make offer')}} <a href="{{lang()}}/auth/login">{{trans('app.Login')}}</a>
                            </h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
