@extends('front.layouts.master')

@section('content')
<div class="course-area section-padding course-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{$page_title}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>

        <div>
            @if(auth()->user()->is_tasker)
            <a href="{{lang()}}/tasks/create" class="btn btn-success">
                {{trans('app.Create')}}
            </a> 
            @endif
            {{trans('app.Filter by status')}} : 
            <a href="{{lang()}}/tasks/my-tasks" class="btn btn-secondary">
                {{trans('app.All')}}
            </a> |
            <a href="{{lang()}}/tasks/my-tasks?status=draft" class="btn btn-secondary">
                {{trans('app.Draft')}}
            </a> | 
            <a href="{{lang()}}/tasks/my-tasks?status=open" class="btn btn-secondary">
                {{trans('app.Open')}}
            </a> | 
            <a href="{{lang()}}/tasks/my-tasks?status=assigned" class="btn btn-secondary">
                {{trans('app.Assigned')}}
            </a> |
            <a href="{{lang()}}/tasks/my-tasks?status=closed" class="btn btn-secondary">
                {{trans('app.Closed')}}
            </a>
        </div>
        <div class="row mt-40 mb-40">
            @if (!$rows->isEmpty())
            @foreach ($rows as $row)
            @include('front.app.single')
            @endforeach
            <div class="container">
                <div class="paganition-center center">
                    {!! $rows->appends(['created_by'=>request('created_by'),'section_id'=>request('section_id')])->render() !!}
                </div>
            </div>
            @else
            {{trans("app.There is no results")}}
            @endif
        </div>
    </div>
</div>
@endsection
