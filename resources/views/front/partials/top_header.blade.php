<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="pull-left">
                    <span>{{trans('app.Phone')}}: {{conf('phone')}}</span>&nbsp;&nbsp;&nbsp;
                    <span>{{trans('app.Email')}}: {{conf('email')}}</span>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="header-top-right pull-right">
                    <div class="content">
                        @if(lang()=='nl')
                        <a href="{{urlLang(url()->full(),lang(),'en')}}" class="nav-link langSwitch">EN</a>
                        @else
                        <a href="{{urlLang(url()->full(),lang(),'nl')}}" class="nav-link langSwitch">NL</a>
                        @endif
                    </div>
                    @include('front.partials.notifications')
                    @include('front.partials.user_nav')
                </div>
            </div>
        </div>
    </div>
</div>