@if(auth()->user())
@php
$notifications = auth()->user()->notifications()->unreaded()->latest()->get();
@endphp
@if(@$notifications)
<div class="content">
    <i class="zmdi zmdi-notifications"></i> <i class="icon ion-ios-bell-outline"></i>
    @if(!@$notifications->isEmpty())
    {{$notifications->count()}}
    @endif
    <ul class="account-dropdown">
        @foreach(@$notifications->take(5) as $notification)
        <li>
            <a href="notifications/to/{{$notification->id}}" class="dropdown-link">
                {{str_limit(strip_tags($notification->message),50)}}
                <br>
                {{$notification->created_at}}
                <!-- media -->
            </a>
        </li>
        @endforeach
        <li>
            <a href="notifications">{{trans('app.Show all')}}</a>
        </li>
        <li>
            <a href="notifications/delete-all" data-confirm="{{trans('notifications.Are you sure you want to delete all notifications')}}?">{{trans('app.Delete all notifications')}}</a>
        </li>
    </ul>
</div>
@endif
@endif