<li class="{{(request()->url()==app()->make("url")->to('/').'/'.lang())?'active':''}}">
    <a href="{{lang()}}">{{trans('app.Home')}}</a>
</li>

<li class="{{request()->is('*/about')?'active':''}}"><a href="{{lang()}}/about">{{trans('app.About')}}</a></li>
<li class="{{request()->is('*/blog')?'active':''}}"><a href="{{lang()}}/blog">{{trans('app.Blog')}}</a></li>
<li class="{{request()->is('*/tasks')?'active':''}}"><a href="{{lang()}}/tasks">{{trans('app.Tasks')}}</a></li>
<li class="{{request()->is('*/contact')?'active':''}}"><a href="{{lang()}}/contact">{{trans('app.Contact')}}</a></li>
