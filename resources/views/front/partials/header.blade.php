<div class="header-logo-menu sticker">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-12">
                <div class="logo pull-left">
                    <a href="{{lang()}}">
                        <img src="uploads/small/{{conf('logo')}}" alt="{{conf('application_name')}}">
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-12">
                <div class="mainmenu-area pull-right">
                    <div class="mainmenu d-none d-lg-block">
                        <nav>
                            <ul id="nav">
                                @include('front.partials.nav') 
                            </ul>
                        </nav>
                    </div>
                    <ul class="header-search">
                        <li class="search-menu">
                            <i id="toggle-search" class="zmdi zmdi-search-for"></i>
                        </li>
                    </ul>
                    <!--Search Form-->
                    <div class="search">
                        <div class="search-form">
                            <form id="search-form" action="{{lang()}}/tasks">
                                <input type="search" placeholder="{{trans('app.Search here')}}..." name="q" />
                                <button type="submit">
                                    <span><i class="fa fa-search"></i></span>
                                </button>
                            </form>                                
                        </div>
                    </div>
                    <!--End of Search Form-->
                </div> 
            </div>
        </div>
    </div>
</div>  
<!-- Mobile Menu Area start -->
<div class="mobile-menu-area">
    <div class="container clearfix">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <ul>
                            @include('front.partials.nav') 
                        </ul>
                    </nav>
                </div>					
            </div>
        </div>
    </div>
</div>
<!-- Mobile Menu Area end --> 