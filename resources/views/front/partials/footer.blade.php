<footer class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-7 col-12">
                <span>{{trans('app.Copyright')}} &copy; {{conf('application_name')}} {{date('Y')}}. {{trans('app.All right reserved, Created by')}} <a href="{{lang()}}">{{conf('application_name')}}</a></span>
            </div>
            <div class="col-lg-6 col-md-5 col-12">
                <div class="column-right">
                    <span>
                        <a href="{{lang()}}/privacy" target="_blank">{{trans('app.Privacy Policy')}} , <a href="{{lang()}}/terms" target="_blank">{{trans('app.Terms and Conditions')}}</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</footer>