<div class="content">
    @if(auth()->user())
        <i class="zmdi zmdi-account"></i> {{trans('app.Welecome')}} {{str_limit(auth()->user()->name,10)}}
        <ul class="account-dropdown">
            @if(auth()->user()->type=='admin')
                <li><a href="{{lang()}}/admin">{{ trans('app.Admin Dashboard') }}</a></li>
            @endif
            <li><a href="{{lang()}}/tasks/my-tasks">{{trans('app.My tasks')}}</a></li>
            <li><a href="{{lang()}}/tasks/my-offers">{{trans('app.My offers')}}</a></li>
            <li><a href="{{lang()}}/transactions">{{trans('app.My transactions')}}</a></li>
            <li><a href="{{lang()}}/favourites">{{trans('app.My favourites')}}</a></li>
            <li><a href="{{lang()}}/posts">{{trans('app.My posts')}}</a></li>
            <li><a href="{{lang()}}/profile/edit">{{trans('app.Edit account')}}</a></li>
            <li><a href="{{lang()}}/profile/change-password">{{trans('app.Change password')}}</a></li>
            <li><a href="{{lang()}}/profile/logout">{{trans('app.Logout')}}</a></li>
        </ul>
    @else
        <a href="{{lang()}}/auth/login">{{trans('app.Login')}}</a> | <a href="{{lang()}}/auth/register">{{trans('app.Register')}}</a>
    @endif
</div>