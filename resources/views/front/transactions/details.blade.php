@extends('front.layouts.master')

@section('content')
    <div class="course-area section-padding course-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title-wrapper">
                        <div class="section-title">
                            <h3>
                                {{trans('app.View transaction')}} #{{$row->id}}
                            </h3>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="container mt-40 mb-40">
                        @if(auth()->user()->id==$row->created_by && $row->status='COMPLETED')
                        <a class="btn btn-sm btn-primary" href="{{lang()}}/tasks/rate/{{$row->task_id}}" title="{{trans('tasks.Rate and Review')}}">
                            <i class="fa fa-star"></i>
                            {{trans('app.Rate tasker')}}
                        </a>
                        @endif
                        <div class="table-responsive">
                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
                                <tr>
                                    <td width="25%" class="align-left">{{trans('transactions.Task')}}</td>
                                    <td width="75%" class="align-left">{{@$row->task->title}}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="align-left">{{trans('transactions.Amount')}}</td>
                                    <td width="75%" class="align-left">{{@$row->amount}} {{trans('app.EUR')}}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="align-left">{{trans('transactions.Tasker')}}</td>
                                    <td width="75%" class="align-left">{{@$row->user->name}}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="align-left">{{trans('transactions.Creator')}}</td>
                                    <td width="75%" class="align-left">{{@$row->creator->name}}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="align-left">{{trans('transactions.Status')}}</td>
                                    <td width="75%" class="align-left">{{@$row->status}}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="align-left">{{trans('transactions.Created at')}}</td>
                                    <td width="75%" class="align-left">{{@$row->created_at}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
