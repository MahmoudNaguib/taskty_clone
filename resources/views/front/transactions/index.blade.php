@extends('front.layouts.master')

@section('content')
    <div class="course-area section-padding course-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title-wrapper">
                        <div class="section-title">
                            <h3>
                                {{$page_title}}
                            </h3>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="container mt-40 mb-40">
                        @if (!$rows->isEmpty())
                            <div class="table-responsive">
                                <table class="table display responsive nowrap dataTable">
                                    <thead>
                                    <tr>
                                        <th class="wd-5p">{{trans('tasks.ID')}} </th>
                                        <th class="wd-10p">{{trans('tasks.Task')}} </th>
                                        <th class="wd-10p">{{trans('tasks.Amount')}} </th>
                                        <th class="wd-10p">{{trans('tasks.Tasker name')}} </th>
                                        <th class="wd-10p">{{trans('tasks.Creator name')}} </th>
                                        <th class="wd-5p">{{trans('tasks.Status')}} </th>
                                        <th class="wd-10p">{{trans('tasks.Created at')}}</th>
                                        <th class="wd-20p">&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($rows as $row)
                                        <tr>
                                            <td class="center">{{$row->id}}</td>
                                            <td class="center">{{$row->task->title}}</td>
                                            <td class="center">{{$row->amount}} {{trans('app.Euro')}}</td>
                                            <td class="center">{{$row->user->name}}</td>
                                            <td class="center">{{$row->creator->name}}</td>
                                            <td class="center">{{$row->status}}</td>
                                            <td class="center">{{str_limit($row->created_at,10,false)}}</td>
                                            <td class="center">
                                                <a class="btn btn-sm btn-primary" href="{{$module}}/details/{{$row->id}}" title="{{trans('tasks.View')}}">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                @if($row->status='COMPLETED')
                                                    <a class="btn btn-sm btn-primary" href="tasks/rate/{{$row->task_id}}" title="{{trans('tasks.Rate and Review')}}">
                                                        <i class="fa fa-star"></i>
                                                        {{trans('app.Rate')}}
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{trans("app.There is no results")}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
