<div class="col-lg-3 col-md-6 col-12">
    <div class="single-teacher-item">
        <div class="single-teacher-image">
            <a href="{{$row->link}}">
                <img src="uploads/small/{{$row->image}}" alt="{{$row->name}}">
            </a>
        </div>
        <div class="single-teacher-text">
            <h3>
                <a href="{{$row->link}}">
                    {{$row->name}}
                </a>
            </h3>
            <p>{{$row->bio_limited}}</p>
            @include('front.bio.partials.share')
        </div>
    </div>
</div>