@extends('front.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="section-title-wrapper">
                <div class="section-title">
                    <h3>{{$page_title}}</h3>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
    <!--News Details Area Start-->
    <div class="news-details-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-12 col-12">
                    <div class="news-details-content">
                        <div class="single-latest-item">
                            @if($row->image)
                                <div class="row">
                                    <div class="col-md-4 offset-md-4">
                                        <img src="uploads/large/{{$row->image}}" alt="{{$row->title}}">
                                    </div>
                                </div>
                            @endif
                            <div class="single-latest-text">
                                <h3>{{$row->name}}</h3>
                                <div class="single-item-comment-view">
                                    @if($row->date_of_birth)
                                        <span>{{trans('app.Date of Birth')}}: <span>{{@$row->date_of_birth}}</span></span>
                                    @endif
                                    @if($row->languages_spoken)
                                        <span>{{trans('app.languages spoken')}}: <span>{{@$row->languages_spoken_fields}}</span></span>
                                    @endif
                                    @if($row->transportation)
                                        <span>{{trans('app.Transportation')}}: <span>{{@$row->transportation_fields}}</span></span>
                                    @endif
                                </div>
                                @if($row->qualifications)
                                    <p>
                                    <h5>{{trans('app.Qualifications')}}</h5>
                                    {{@$row->qualifications}}
                                    </p>
                                @endif

                                @if($row->work_experience)
                                    <p>
                                    <h5>{{trans('app.Work experience')}}</h5>
                                    {{@$row->work_experience}}
                                    </p>
                                @endif

                                <p>{!! $row->bio !!}</p>
                                <div class="tags-and-links">
                                    <div class="social-links">
                                        <span>{{trans('app.Share')}}:</span>
                                        <a href="https://facebook.com/sharer/sharer.php?u={{$row->link}}" target="_blank"><i
                                                class="zmdi zmdi-facebook"></i></a>
                                        <a href="https://twitter.com/intent/tweet?url={{$row->link}}" target="_blank"><i
                                                class="zmdi zmdi-star"></i></a>
                                        <a href="https://www.linkedin.com/shareArticle?url={{$row->link}}" target="_blank"><i
                                                class="zmdi zmdi-linkedin"></i></a>
                                        <a href="https://web.whatsapp.com/send?text={{$row->link}}" target="_blank"><i class="zmdi zmdi-whatsapp"></i></a>
                                    </div>
                                    @if(!$row->rates->isEmpty())
                                        <div class="social-links">
                                            <span>{{trans('app.Rate')}}:</span>
                                            @php
                                                $rate=ceil($row->rates->sum('value')/$row->rates->count());
                                                for ($i=0; $i<$rate;$i++){
                                                    echo '<i class="zmdi zmdi-star"></i>';
                                                }
                                            @endphp
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="comments">
                            <h4 class="title">{{trans('app.Reviews')}}</h4>
                            @if(!$row->rates->isEmpty())
                                @foreach($row->rates as $rate)
                                    <div class="single-comment">
                                        <div class="author-image">
                                            <a href="{{@$rate->creator->link}}">
                                                <img src="uploads/small/{{$rate->creator->image}}"
                                                     alt="{{@$rate->creator->name}}-{{$rate->id}}">
                                            </a>
                                        </div>
                                        <div class="comment-text">
                                            <div class="author-info">
                                                <h4><a href="#">{{$rate->creator->name}}</a></h4>
                                                <span class="comment-time">{{trans('app.Posted on')}} {{$rate->created_at}}</span>
                                            </div>
                                            <p>{{$rate->review}}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                {{trans('app.There is no reviews')}}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-12 col-12">
                    <div class="sidebar-widget">
                        @php
                            $tasks=\App\Models\Task::open()->where('created_by',$row->id)->latest()->limit(5)->get();
                        @endphp
                        @if($tasks)
                            <div class="single-sidebar-widget">
                                <h4 class="title">{{trans('app.Tasks')}}</h4>
                                @foreach($tasks as $task)
                                    <div class="recent-content">
                                        <div class="recent-content-item">
                                            <a href="#">
                                                <img src="uploads/small/{{$task->image}}" alt="{{$task->title}}" width='50'>
                                            </a>
                                            <div class="recent-text">
                                                <h4><a href="{{$task->link}}">{{$task->title_limited}}</a></h4>
                                                <div class="single-item-comment-view">
                                                </div>
                                                <p>{{$task->content_limited}}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
@endsection
