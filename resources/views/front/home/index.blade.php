@extends('front.layouts.master')

@section('content')
<!--End of Header Area-->
<!--Background Area Start-->
<div class="background-area no-animation">	
    <img src="uploads/large/{{conf('home_banner')}}" alt="{{conf('home_banner_text')}}"/>
    <div class="banner-content static-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-content-wrapper full-width">
                        <div class="text-content text-center">
                            <h1 class="title1 text-center mb-20">
                                <span class="tlt block" data-in-effect="rollIn" data-out-effect="fadeOutRight" >{{conf('home_banner_text')}}</span>
                            </h1>
                            <div class="banner-readmore wow bounceInUp" data-wow-duration="2500ms" data-wow-delay=".1s">
                                <a class="button-default" href="{{lang()}}/tasks">{{trans('app.View tasks')}}</a>	                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>	 
    </div>
</div>
<!--End of Background Area-->
<!--About Area Start-->
<div class="about-area" style="background: rgba(0, 0, 0, 0) url(uploads/large/{{conf('why_banner')}}) no-repeat scroll center top;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="about-container">
                    <h3>{{trans('app.Why us')}}?</h3>
                    <p>
                        {{conf('home_why_text')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!--End of About Area-->
@if(@$viewedTasks && !@$viewedTasks->isEmpty())
<!--Course Area Start-->
<div class="course-area section-padding mt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('app.Latest viewed tasks')}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($viewedTasks as $row)
            @include('front.tasks.single')
            @endforeach
        </div>
    </div>
</div>
<!--End of Course Area-->
@endif


@if(@$relatedTasks && !@$relatedTasks->isEmpty())
<!--Course Area Start-->
<div class="course-area section-padding mt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('app.Related tasks')}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($relatedTasks as $row)
            @include('front.tasks.single')
            @endforeach
        </div>
    </div>
</div>
<!--End of Course Area-->
@endif



<!--End of About Area-->
@if(@$tasks && !@$tasks->isEmpty())
<!--Course Area Start-->
<div class="course-area section-padding mt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('app.Latest tasks')}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($tasks as $row)
            @include('front.tasks.single')
            @endforeach
            <div class="col-md-12 col-sm-12 text-center">
                <a href="{{lang()}}/tasks" class="button-default button-large">{{trans('app.Browse all tasks')}} <i class="zmdi zmdi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
<!--End of Course Area-->
@endif







<!--Fun Factor Area Start-->
<div class="fun-factor-area mt-30" style="background: rgba(0, 0, 0, 0) url(uploads/large/{{conf('facts_banner')}}) repeat scroll 0 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper white">
                    <div class="section-title">
                        <h3>{{trans('app.IMPORTANT FACTS')}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="single-fun-factor">
                    <h4>{{trans('app.Members')}}</h4>
                    <h2><span class="counter">{{\App\Models\User::active()->get()->count()}}</span>+</h2>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="single-fun-factor">
                    <h4>{{trans('app.Tasker')}}</h4>
                    <h2><span class="counter">{{\App\Models\User::active()->where('is_tasker',1)->get()->count()}}</span>+</h2>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="single-fun-factor">
                    <h4>{{trans('app.Open Tasks')}}</h4>
                    <h2><span class="counter">{{\App\Models\Task::open()->get()->count()}}</span>+</h2>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="single-fun-factor">
                    <h4>{{trans('app.Closed Tasks')}}</h4>
                    <h2><span class="counter">{{\App\Models\Task::closed()->where('status','closed')->get()->count()}}</span>+</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Fun Factor Area-->   
<!--Latest News Area Start-->
@if(@$posts && !@$posts->isEmpty())
<div class="latest-area section-padding bg-white mt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('app.Latest News')}}</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($posts as $row)
            @include('front.blog.single')
            @endforeach
            <div class="col-md-12 col-sm-12 text-center">
                <a href="{{lang()}}/posts" class="button-default button-large">{{trans('app.Browse all posts')}} <i class="zmdi zmdi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
@endif
<!--End of Latest News Area--> 

@if(@$taskers && !@$taskers->isEmpty())
<!--Teachers Area Start-->
<div class="teachers-area padding-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('app.Our tasker')}}</h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach($taskers as $row)
            @include('front.bio.single')
            @endforeach
            <div class="col-md-12 col-sm-12 text-center">
                <a href="{{lang()}}/bio" class="button-default button-large">{{trans('app.Browse all taskers')}} <i class="zmdi zmdi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
<!--End of Teachers Area-->
@endif
@endsection
