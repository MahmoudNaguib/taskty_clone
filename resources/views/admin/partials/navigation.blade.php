<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            <li class="nav-item {{(request()->is(lang()))?"active":""}}">
                <a class="nav-link" href="{{app()->make("url")->to('/')}}/{{lang()}}/admin">
                    <i class="icon ion-ios-pie-outline"></i>
                    <span>{{trans('navigation.Dashboard')}}</span>
                </a>
            </li>

            @if(can('create-users') || can('view-users'))
                <li class="nav-item {{(request()->is('*/users*'))?"active":""}}">
                    <a class="nav-link" href="{{lang()}}/admin/users">
                        <i class="icon fa ion-ios-contact-outline"></i>
                        <span>{{trans('navigation.Users')}}</span>
                    </a>
                </li>
            @endif

            @if(can('create-sections') || can('view-sections') || can('create-tasks') || can('view-tasks'))
                <li class="nav-item with-sub settings {{(request()->is('*/sections*') || request()->is('*/tasks*'))?"active":""}}">
                    <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
                        <i class="icon fa ion-ios-compose"></i>
                        <span>{{trans('navigation.Tasks')}} & {{trans('navigation.Sections')}}</span>
                    </a>
                    <div class="sub-item">
                        <ul>
                            @if(can('create-sections') || can('view-sections'))
                                <li class="{{(request()->is('*/sections*'))?"active":""}}">
                                    <a href="{{lang()}}/admin/sections">{{trans('navigation.Sections')}}</a>
                                </li>
                            @endif

                            @if(can('create-tasks') || can('view-tasks'))
                                <li class="{{(request()->is('*/tasks*'))?"active":""}}">
                                    <a href="{{lang()}}/admin/tasks">{{trans('navigation.Tasks')}}</a>
                                </li>
                            @endif

                            @if(can('view-transactions'))
                                <li class="{{(request()->is('*/transactions*'))?"active":""}}">
                                    <a href="{{lang()}}/admin/transactions">{{trans('navigation.Transactions')}}</a>
                                </li>
                            @endif
                        </ul>
                    </div><!-- dropdown-menu -->
                </li>
            @endif


            @if(can('create-posts') || can('view-posts'))
                <li class="nav-item {{(request()->is('*/posts*'))?"active":""}}">
                    <a class="nav-link" href="{{lang()}}/admin/posts">
                        <i class="icon fa ion-ios-compose"></i>
                        <span>{{trans('navigation.Posts')}}</span>
                    </a>
                </li>
            @endif

            @if(can('create-faqs') || can('view-faqs'))
                <li class="nav-item {{(request()->is('*/faqs*'))?"active":""}}">
                    <a class="nav-link" href="{{lang()}}/admin/faqs">
                        <i class="icon fa ion-ios-compose"></i>
                        <span>{{trans('navigation.Faqs')}}</span>
                    </a>
                </li>
            @endif


            <li class="nav-item with-sub settings">
                <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
                    <i class="icon ion-ios-gear-outline"></i>
                    <span>{{trans('navigation.Settings')}}</span>
                </a>
                <div class="sub-item">
                    <ul>
                        @if(@auth()->user()->is_super_admin)
                            <li class="{{(request()->is('*/configs*'))?"active":""}}">
                                <a href="{{lang()}}/admin/configs">{{trans('navigation.Configurations')}}</a>
                            </li>
                        @endif
                        @if(@auth()->user()->is_super_admin)
                            <li class="{{(request()->is('*/roles*'))?"active":""}}">
                                <a href="{{lang()}}/admin/roles">{{trans('navigation.Roles')}}</a>
                            </li>
                        @endif
                        @if(can('view-countries') || can('create-countries'))
                            <li class="{{(request()->is('*/countries*'))?"active":""}}">
                                <a href="{{lang()}}/admin/countries">{{trans('navigation.Countries')}}</a>
                            </li>
                        @endif
                        @if(can('view-contact_messages'))
                            <li class="{{(request()->is('*/contact_messages*'))?"active":""}}">
                                <a href="{{lang()}}/admin/contact_messages">{{trans('navigation.Contact messages')}}</a>
                            </li>
                        @endif
                        @if(can('view-subscribers'))
                            <li class="{{(request()->is('*/subscribers*'))?"active":""}}">
                                <a href="{{lang()}}/admin/subscribers">{{trans('navigation.Newsletter subscribers')}}</a>
                            </li>
                        @endif

                        @if(@auth()->user()->is_super_admin)
                            <li class="{{(request()->is('*/translator*'))?"active":""}}">
                                <a href="{{lang()}}/admin/translator">{{trans('navigation.Localization')}}</a>
                            </li>
                        @endif

                    </ul>
                </div><!-- dropdown-menu -->
            </li>

        </ul>
    </div>
    <!-- container -->
</div>