@if(can('view-'.$questionsModule))
@if (!$records->isEmpty())
<div class="table-responsive mg-t-20">
    <a href="{{$questionsModule}}/export?task_id={{$row->id}}" class="btn btn-primary">
        <i class="fa fa-arrow-down"></i> {{trans('app.Export')}}
    </a>
    <br><br>
    <table class="table display responsive nowrap dataTable">
        <thead>
            <tr>
                <th class="wd-10p">{{trans('tasks.ID')}} </th>
                <th class="wd-50p">{{trans('tasks.Content')}} </th>
                <th class="wd-15p">{{trans('tasks.Created by')}} </th>
                <th class="wd-15p">{{trans('tasks.Created at')}}</th>
                <th class="wd-15p">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($records as $record)
            <tr>
                <td class="center">{{$record->id}}</td>
                <td class="center">{!! wordwrap(@$record->content,75,'<br>',true) !!}</td>
                <td class="center">{{$record->creator->name}}</td>
                <td class="center">{{$record->created_at}}</td>
                <td class="center">
                    <a class="btn btn-primary btn-xs" href="{{$questionsModule}}/view/{{$record->id}}" title="{{trans('tasks.View')}}" data-toggle="modal" data-target="#questions-view-{{$record->id}}"><i class="fa fa-eye"></i>
                    </a>
                    <div class="modal fade" id="questions-view-{{$record->id}}" role="document">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content bd-0">
                                <div class="modal-header pd-y-20 pd-x-25">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{trans('tasks.View')}} {{trans('tasks.Questions')}}</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body pd-25">
                                    @include('admin.questions.partials.attributes')
                                </div>
                            </div>
                        </div><!-- modal-dialog -->
                    </div>
                    @if(can('delete-'.$questionsModule))
                    <a class="btn btn-danger btn-xs" href="{{$questionsModule}}/delete/{{$row->id}}" title="{{trans('tasks.Delete')}}" data-confirm="{{trans('tasks.Are you sure you want to delete this item')}}?">
                        <i class="fa fa-trash"></i>
                    </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@else
{{trans("tasks.There is no results")}}
@endif
@endif