<div class="mg-b-10">
    {!! Form::model($row,['method' => 'get','files' => true] ) !!} 
    <div class="row">
        <div class="col-lg-3 col-md-6 mg-t-10">
            {!! Form::select('status',  $row->getStatusValues(),@request('status'), ['class'=>'form-control','placeholder'=>trans('tasks.Status')]) !!}
        </div><!-- col-4 -->
        <div class="col-lg-3 col-md-6 mg-t-10">
            {!! Form::select('section_id',  $row->getSectionsWithParents(),@request('section_id'), ['class'=>'form-control','placeholder'=>trans('tasks.Section')]) !!}
        </div><!-- col-4 -->
        <div class="col-lg-3 col-md-6 mg-t-10">
            {!! Form::select('where',  $row->getWhereValues(),@request('where'), ['class'=>'form-control','placeholder'=>trans('tasks.Where')]) !!}
        </div><!-- col-4 -->
        <div class="col-lg-3 col-md-6 mg-t-10">
            {!! Form::select('country_id',  $row->getCountries(),@request('country_id'), ['class'=>'form-control','placeholder'=>trans('tasks.Country')]) !!}
        </div><!-- col-4 -->
        <div class="col-lg-2 col-md-6 mg-t-10">
            {!! Form::text('from_date',@request('from_date'),['class'=>'form-control datepicker','placeholder'=>trans('tasks.From'),'autocomplete'=>'off']) !!}
        </div>
        <div class="col-lg-2 col-md-6 mg-t-10">
            {!! Form::text('to_date',@request('to_date'),['class'=>'form-control datepicker','placeholder'=>trans('tasks.To'),'autocomplete'=>'off']) !!}
        </div>
        <div class="col-lg-3 col-md-6 mg-t-10">
            {!! Form::select('time',  $row->getTimeValues(),@request('time'), ['class'=>'form-control','placeholder'=>trans('tasks.Time')]) !!}
        </div><!-- col-4 -->
        <div class="col-lg-2 col-md-6 mg-t-10">
            {!! Form::number('budget',@request('budget'),['class'=>'form-control','placeholder'=>trans('tasks.Budget').' '.trans('tasks.Less than')]) !!}
        </div>

        
        <div class="col-lg-3 col-md-6 mg-t-10">
            <button class="btn btn-primary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Filter') }}</button> 
            <a href="{{$module}}" class="btn btn-primary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Reset') }}</a>
        </div>
    </div><!-- row -->
    {!! Form::close() !!}
</div><!-- section-wrapper -->