@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
    @if(can('create-'.$module))
    <a href="{{$module}}/create" class="btn btn-success">
        <i class="fa fa-plus"></i> {{trans('app.Create')}}
    </a>
    @endif
    @if(can('view-'.$module))
    <a href="{{$module}}/export?{{@$_SERVER['QUERY_STRING']}}" class="btn btn-primary">
        <i class="fa fa-arrow-down"></i> {{trans('app.Export')}}
    </a>
    @endif
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('view-'.$module))
    @include($module.'.partials.filters')
    @if (!$rows->isEmpty())
    <div class="table-responsive">
        <table class="table display responsive nowrap">
            <thead>
                <tr>
                    <th class="wd-5p">{{trans('tasks.ID')}} </th>
                    <th class="wd-10p">{{trans('tasks.Section')}} </th>
                    <th class="wd-10p">{{trans('tasks.Title')}} </th>
                    <th class="wd-5p">{{trans('tasks.Where')}} </th>
                    <th class="wd-10p">{{trans('tasks.Date')}} </th>
                    <th class="wd-10p">{{trans('tasks.Time')}} </th>
                    <th class="wd-5p">{{trans('tasks.Status')}} </th>
                    <th class="wd-5p">{{trans('tasks.Views')}} </th>
                    <th class="wd-10p">{{trans('tasks.Created by')}} </th>
                    <th class="wd-10p">{{trans('tasks.Created at')}}</th>
                    <th class="wd-20p">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rows as $row)
                <tr>
                    <td class="center">{{$row->id}}</td>
                    <td class="center">{{$row->section->title}}</td>
                    <td class="center">{{str_limit($row->title,50)}}</td>
                    <td class="center">{{$row->where_value}}</td>
                    <td class="center">{{$row->date}}</td>
                    <td class="center">{{$row->time_value}}</td>
                    <td class="center">{{$row->status_value}}</td>
                    <td class="center">{{$row->views}}</td>
                    <td class="center">{{@$row->creator->name}}</td>
                    <td class="center">{{str_limit($row->created_at,10,false)}}</td>
                    <td class="center">
                        @if(can('edit-'.$module))
                        <a class="btn btn-success btn-xs" href="{{$module}}/edit/{{$row->id}}" title="{{trans('posts.Edit')}}">
                            <i class="fa fa-edit"></i>
                        </a>
                        @endif
                        @if(can('view-'.$module))
                        <a class="btn btn-primary btn-xs" href="{{$module}}/view/{{$row->id}}" title="{{trans('tasks.View')}}">
                            <i class="fa fa-eye"></i>
                        </a>
                        @endif

                        @if(can('delete-'.$module))
                        <a class="btn btn-danger btn-xs" href="{{$module}}/delete/{{$row->id}}" title="{{trans('tasks.Delete')}}" data-confirm="{{trans('tasks.Are you sure you want to delete this item')}}?">
                            <i class="fa fa-trash"></i>
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="paganition-center"> 
        {!! $rows->appends(['status'=>request('status'),'section_id'=>request('section_id'),'where'=>request('where'),'country_id'=>request('country_id'),'from_date'=>request('from_date'),'to_date'=>request('to_date'),'time'=>request('time'),'budget'=>request('budget')])->render() !!}
    </div>
    @else
    {{trans("tasks.There is no results")}}
    @endif
    @endif
</div>
@endsection
