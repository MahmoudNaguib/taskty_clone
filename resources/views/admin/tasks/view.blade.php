@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('edit-'.$module))
    <a href="{{$module}}/edit/{{$row->id}}" class="btn btn-success">
        <i class="fa fa-edit"></i> {{trans('posts.Edit')}}
    </a><br>
    @endif
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-details-tab" data-toggle="tab" href="#nav-details" role="tab" aria-controls="nav-details" aria-selected="true">Details</a>
            <a class="nav-item nav-link" id="nav-questions-tab" data-toggle="tab" href="#nav-offers" role="tab" aria-controls="nav-offers" aria-selected="false">Offers</a>
            <a class="nav-item nav-link" id="nav-questions-tab" data-toggle="tab" href="#nav-questions" role="tab" aria-controls="nav-questions" aria-selected="false">Questions</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-details" role="tabpanel" aria-labelledby="nav-details-tab">
            <div class="table-responsive mg-t-20">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Section')}} </td>
                        <td width="75%" class="align-left">{{@$row->section->title}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Title')}} </td>
                        <td width="75%" class="align-left">{{@$row->title}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Slug')}} </td>
                        <td width="75%" class="align-left">{{@$row->slug}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Content')}} </td>
                        <td width="75%" class="align-left">{{@$row->content}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Where')}} </td>
                        <td width="75%" class="align-left">{{@$row->where_value}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Country')}} </td>
                        <td width="75%" class="align-left">{{@$row->country->title}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Location')}} </td>
                        <td width="75%" class="align-left">{{@$row->location}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Map location')}} </td>
                        <td width="75%" class="align-left"><a href='https://www.google.com/maps/place/{{$row->latitude}},{{@$row->longitude}}' target="_blank">{{trans('tasks.Location')}}</a></td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Date')}} </td>
                        <td width="75%" class="align-left">{{@$row->date}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Time')}} </td>
                        <td width="75%" class="align-left">{{@$row->time_value}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Budget')}} </td>
                        <td width="75%" class="align-left">{{@$row->budget}} {{trans('tasks.EUR')}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Status')}} </td>
                        <td width="75%" class="align-left">{{@$row->status_value}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Tags')}}</td>
                        <td width="75%" class="align-left">{{$row->tags}}</td>
                    </tr>
                    @if($row->attachment)
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Attachments')}} </td>
                        <td width="75%" class="align-left">{!! fileRender($row->attachment) !!}</td>
                    </tr>
                    @endif
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Views')}} </td>
                        <td width="75%" class="align-left">{{@$row->views}} {{trans('tasks.Views')}}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="align-left">{{trans('tasks.Created by')}}</td>
                        <td width="75%" class="align-left">{{@$row->creator->name}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-offers" role="tabpanel" aria-labelledby="nav-offers-tab">
            @include($module.'.partials.offers',['records'=>$row->offers])
        </div>
        <div class="tab-pane fade" id="nav-questions" role="tabpanel" aria-labelledby="nav-questions-tab">
            @include($module.'.partials.questions',['records'=>$row->questions])
        </div>
    </div>



</div>
@endsection
