@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
     @include($module.'.partials.attributes',['record'=>$row])
</div>
@endsection
