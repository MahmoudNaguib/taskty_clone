<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
        <tr>
            <td width="25%" class="align-left">{{trans('tasks.Content')}}</td>
            <td width="75%" class="align-left">{!! wordwrap(@$record->content,75,'<br>',true) !!}</td>
        </tr>
        @if($record->attachment)
        <tr>
            <td width="25%" class="align-left">{{trans('tasks.Attachment')}}</td>
            <td width="75%" class="align-left">{!! fileRender($record->attachment) !!}</td>
        </tr>
        @endif
        <tr>
            <td width="25%" class="align-left">{{trans('tasks.Created by')}}</td>
            <td width="75%" class="align-left">{{$record->creator->name}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('tasks.Created at')}}</td>
            <td width="75%" class="align-left">{{$record->created_at}}</td>
        </tr>

    </table>
</div>