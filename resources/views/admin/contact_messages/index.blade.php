@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
    @if(can('view-'.$module))
    <a href="{{$module}}/export?{{@$_SERVER['QUERY_STRING']}}" class="btn btn-primary">
        <i class="fa fa-download"></i> {{trans('app.Export')}}
    </a>
    @endif
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('view-'.$module))
    @if (!$rows->isEmpty())
    <div class="table-responsive">
        <table class="table display responsive nowrap dataTable">
            <thead>
                <tr>
                    <th class="wd-10p">{{trans('contact_messages.ID')}} </th>
                    <th class="wd-10p">{{trans('contact_messages.Name')}} </th>
                    <th class="wd-10p">{{trans('contact_messages.Mobile')}} </th>
                    <th class="wd-10p">{{trans('contact_messages.Email')}} </th>
                    <th class="wd-25p">{{trans('contact_messages.Content')}} </th>
                    <th class="wd-15p">{{trans('contact_messages.Created at')}}</th>
                    <th class="wd-15p">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rows as $row)
                <tr>
                    <td class="center">{{$row->id}}</td>
                    <td class="center">{{$row->name}}</td>
                    <td class="center">{{$row->mobile}}</td>
                    <td class="center">{{$row->email}}</td>
                    <td class="center">{!! str_limit($row->content,20) !!}</td>
                    <td class="center">{{$row->created_at}}</td>
                    <td class="center">

                        @if(can('view-'.$module))
                        <a class="btn btn-primary btn-xs" href="{{$module}}/view/{{$row->id}}" title="{{trans('contact_messages.View')}}">
                            <i class="fa fa-eye"></i>
                        </a>
                        @endif

                        @if(can('delete-'.$module))
                        <a class="btn btn-danger btn-xs" href="{{$module}}/delete/{{$row->id}}" title="{{trans('contact_messages.Delete')}}" data-confirm="{{trans('contact_messages.Are you sure you want to delete this item')}}?">
                            <i class="fa fa-trash"></i>
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @else
    {{trans("contact_messages.There is no results")}}
    @endif
    @endif
</div>
@endsection