@include('form.select',['name'=>'role_id','options'=>$row->getRoles(),'attributes'=>['id'=>'role_id','class'=>'form-control','label'=>trans('users.Role'),'placeholder'=>trans('users.Role')]])

@include('form.input',['name'=>'name','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Name'),'placeholder'=>trans('users.Name'),'required'=>1]])

@include('form.input',['name'=>'email','type'=>'email','attributes'=>['class'=>'form-control','label'=>trans('users.Email'),'placeholder'=>trans('users.Email'),'required'=>1]])

@include('form.input',['name'=>'mobile','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Mobile'),'placeholder'=>trans('users.Mobile'),'required'=>1]])

@include('form.select',['name'=>'language','options'=>langs(),'attributes'=>['class'=>'form-control','label'=>trans('users.Default language'),'placeholder'=>trans('users.Default language'),'required'=>1]])

@include('form.select',['name'=>'gender','options'=>$row->getGenders(),'attributes'=>['class'=>'form-control','label'=>trans('users.Gender'),'placeholder'=>trans('users.Gender'),'required'=>1]])

 @include('form.input',['name'=>'date_of_birth','type'=>'text','attributes'=>['class'=>'form-control datepicker','label'=>trans('users.Date of Birth'),'placeholder'=>trans('users.Date of Birth'),'required'=>1,'autocomplete'=>'off']])

@php
$attributes=['class'=>'form-control','label'=>trans('users.Password'),'placeholder'=>trans('users.Password'),'required'=>1];
if(@$row->id) unset($attributes['required']);
@endphp
@include('form.password',['name'=>'password','attributes'=>$attributes])

@php
$attributes=['class'=>'form-control','label'=>trans('users.Password confirmation'),'placeholder'=>trans('users.Password confirmation'),'required'=>1];
if(@$row->id) unset($attributes['required']);
@endphp

@include('form.password',['name'=>'password_confirmation','attributes'=>$attributes])

@include('form.boolean',['name'=>'is_tasker','attributes'=>['label'=>trans('users.Is tasker')]])

@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('users.Is active')]])

@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('users.Image'),'placeholder'=>trans('users.Image')]])

<h4>{{trans('users.Address information')}}</h4>

@include('form.select',['name'=>'country_id','options'=>$row->getCountries(),'attributes'=>['id'=>'country_code','class'=>'form-control','label'=>trans('users.Country'),'placeholder'=>trans('users.Country'),'required'=>1]])

@include('form.input',['name'=>'city','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.City'),'placeholder'=>trans('users.City'),'required'=>1]])

@include('form.input',['name'=>'street_name','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Street name'),'placeholder'=>trans('users.Street name'),'required'=>1]])

@include('form.input',['name'=>'house_number','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.House number'),'placeholder'=>trans('users.House number'),'required'=>1]])

@include('form.input',['name'=>'postal_code','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Postal code'),'placeholder'=>trans('users.Postal code'),'required'=>1]])

<h4>{{trans('users.Bio information')}}</h4>

@include('form.input',['name'=>'bio','type'=>'textarea','attributes'=>['class'=>'form-control editor','label'=>trans('users.Bio'),'placeholder'=>trans('users.Bio')]])

@include('form.select',['name'=>'languages_spoken[]','options'=>$row->getLanguages(),'attributes'=>['class'=>'form-control select2','label'=>trans('users.Language spoken'),'multiple'=>true]])

@include('form.select',['name'=>'transportation[]','options'=>$row->getTransportations(),'attributes'=>['class'=>'form-control select2','label'=>trans('users.Transportation'),'multiple'=>true]])

@include('form.input',['name'=>'qualifications','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Qualifications'),'placeholder'=>trans('users.Qualifications')]])

@include('form.input',['name'=>'work_experience','type'=>'textarea','attributes'=>['class'=>'form-control','label'=>trans('users.Work experience'),'placeholder'=>trans('users.Work experience')]])

@include('form.boolean',['name'=>'id_badge','attributes'=>['label'=>trans('users.ID Badge')]])

@include('form.boolean',['name'=>'license_badge','attributes'=>['label'=>trans('users.License Badge')]])

@include('form.boolean',['name'=>'partner_badge','attributes'=>['label'=>trans('users.Partner Badge')]])

@include('form.boolean',['name'=>'working_with_children_badge','attributes'=>['label'=>trans('users.Working with children Badge')]])




<h4>{{trans('users.Bank information')}}</h4>
    @include('form.input',['name'=>'bank_name','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Bank name'),'placeholder'=>trans('users.Bank name')]])

    @include('form.input',['name'=>'bank_account_number','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Bank account number'),'placeholder'=>trans('users.Bank account number')]])

    @include('form.input',['name'=>'swift_code','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Swift code'),'placeholder'=>trans('users.Swift code')]])
