@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('edit-'.$module))
    <a href="{{$module}}/edit/{{$row->id}}" class="btn btn-success">
        <i class="fa fa-edit"></i> {{trans('users.Edit')}}
    </a><br>
    @endif
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
            @if(@$row->role_id)
            <tr>
                <td width="25%" class="align-left">{{trans('users.Role')}}</td>
                <td width="75%" class="align-left">{{@$row->role->title}}</td>
            </tr>
            @endif
            <tr>
                <td width="25%" class="align-left">{{trans('users.Type')}}</td>
                <td width="75%" class="align-left">{{@$row->type}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Name')}}</td>
                <td width="75%" class="align-left">{{@$row->name}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Email')}}</td>
                <td width="75%" class="align-left">{{@$row->email}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Mobile')}}</td>
                <td width="75%" class="align-left">{{@$row->mobile}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Language')}}</td>
                <td width="75%" class="align-left">{{@$row->language}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Date of Birth')}}</td>
                <td width="75%" class="align-left">{{@$row->date_of_birth}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Image')}}</td>
                <td width="75%" class="align-left">{!! image($row->image,'small') !!}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Is admin')}}</td>
                <td width="75%" class="align-left">{{($row->is_admin)?trans('users.Yes'):trans('users.No')}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Is tasker')}}</td>
                <td width="75%" class="align-left">{{($row->is_tasker)?trans('users.Yes'):trans('users.No')}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Bio')}}</td>
                <td width="75%" class="align-left">{!! @$row->bio!!}</td>
            </tr>

            <tr>
                <td width="25%" class="align-left">{{trans('users.languages spoken')}}</td>
                <td width="75%" class="align-left">{{@$row->languages_spoken_fields}}</td>
            </tr>

            <tr>
                <td width="25%" class="align-left">{{trans('users.Transportation')}}</td>
                <td width="75%" class="align-left">{{@$row->getTransportations()[@$row->transportation]}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Qualifications')}}</td>
                <td width="75%" class="align-left">{{@$row->qualifications}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Work experience')}}</td>
                <td width="75%" class="align-left">{{@$row->work_experience}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Bank name')}}</td>
                <td width="75%" class="align-left">{{@$row->bank_name}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Bank account number')}}</td>
                <td width="75%" class="align-left">{{@$row->bank_account_number}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('users.Swift code')}}</td>
                <td width="75%" class="align-left">{{@$row->swift_code}}</td>
            </tr>
            @if(@$row->creator->name)
            <tr>
                <td width="25%" class="align-left">{{trans('users.Created by')}}</td>
                <td width="75%" class="align-left">{{@$row->creator->name}}</td>
            </tr>
            @endif
        </table>
    </div>
</div>
@endsection
