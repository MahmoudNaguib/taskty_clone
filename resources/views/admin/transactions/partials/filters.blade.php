{!! Form::model($row,['method' => 'get','files' => true] ) !!}
<div class="row">

    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        {!! Form::text('from_date',@request('from_date'),['class'=>'form-control datepicker','placeholder'=>trans('app.From'),'autocomplete'=>'off']) !!}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        {!! Form::text('to_date',@request('to_date'),['class'=>'form-control datepicker','placeholder'=>trans('app.To'),'autocomplete'=>'off']) !!}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        {!! Form::number('amount_from',@request('amount_from'),['class'=>'form-control','placeholder'=>trans('app.Amount from'),'min'=>0]) !!}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        {!! Form::number('amount_to',@request('amount_to'),['class'=>'form-control','placeholder'=>trans('app.Amount to'),'min'=>0]) !!}
    </div>
    <div class="col-lg-3 col-md-6 mg-t-10">
        {!! Form::select('status',  ['COMPLETED'=>'COMPLETED'],@request('status'), ['class'=>'form-control','placeholder'=>trans('app.Status')]) !!}
    </div>
    <div class="col-lg-3 col-md-6 mg-t-10">
        {!! Form::select('user_id', $row->getUsers(),@request('user_id'), ['class'=>'form-control select2','placeholder'=>trans('app.Users')]) !!}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mt-20">
        <button class="btn btn-theme btn-secondary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Filter') }}</button>
        <a href="{{$module}}" class="btn btn-theme btn-secondary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Reset') }}</a>
    </div>
</div>
{!! Form::close() !!}
