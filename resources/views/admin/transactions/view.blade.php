@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
            <tr>
                <td width="25%" class="align-left">{{trans('transactions.Task')}}</td>
                <td width="75%" class="align-left">{{@$row->task->title}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('transactions.Amount')}}</td>
                <td width="75%" class="align-left">{{@$row->amount}} {{trans('app.EUR')}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('transactions.Tasker')}}</td>
                <td width="75%" class="align-left">{{@$row->user->name}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('transactions.Creator')}}</td>
                <td width="75%" class="align-left">{{@$row->creator->name}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('transactions.Status')}}</td>
                <td width="75%" class="align-left">{{@$row->status}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('transactions.Created by')}}</td>
                <td width="75%" class="align-left">{{@$row->creator->name}}</td>
            </tr>
        </table>
    </div>
</div>
@endsection
