@extends('admin.layouts.master')
@section('title')
    <h6 class="slim-pagetitle">
        {{ @$page_title }}
        @if(can('view-'.$module))
            <a href="{{$module}}/export?{{@$_SERVER['QUERY_STRING']}}" class="btn btn-primary">
                <i class="fa fa-arrow-down"></i> {{trans('app.Export')}}
            </a>
        @endif
    </h6>
@endsection
@section('content')
    <div class="section-wrapper">
        @if(can('view-'.$module))
            @include($module.'.partials.filters')
            @if (!$rows->isEmpty())
                <div class="table-responsive">
                    <table class="table display responsive nowrap">
                        <thead>
                        <tr>
                            <th class="wd-5p">{{trans('transactions.ID')}} </th>
                            <th class="wd-25p">{{trans('transactions.Task')}} </th>
                            <th class="wd-10p">{{trans('transactions.Amount')}}</th>
                            <th class="wd-10p">{{trans('transactions.Tasker')}}</th>
                            <th class="wd-10p">{{trans('transactions.Creator')}}</th>
                            <th class="wd-10p">{{trans('transactions.Status')}}</th>
                            <th class="wd-15p">{{trans('transactions.Created at')}}</th>
                            <th class="wd-20p">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($rows as $row)
                            <tr>
                                <td class="center">{{$row->id}}</td>
                                <td class="center">{{str_limit($row->task->title,50)}}</td>
                                <td class="center">{{$row->amount}} {{trans('app.EUR')}}</td>
                                <td class="center">{{@$row->user->name}}</td>
                                <td class="center">{{@$row->creator->name}}</td>
                                <td class="center">{{@$row->status}}</td>
                                <td class="center">{{$row->created_at}}</td>
                                <td class="center">

                                    @if(can('view-'.$module))
                                        <a class="btn btn-primary btn-xs" href="{{$module}}/view/{{$row->id}}" title="{{trans('transactions.View')}}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    @endif

                                    @if(can('delete-'.$module))
                                        <a class="btn btn-danger btn-xs" href="{{$module}}/delete/{{$row->id}}"
                                           title="{{trans('transactions.Delete')}}"
                                           data-confirm="{{trans('transactions.Are you sure you want to delete this item')}}?">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="paganition-center">
                    {!! $rows->appends([])->render() !!}
                </div>
            @else
                {{trans("transactions.There is no results")}}
            @endif
        @endif
    </div>
@endsection
