@extends('admin.layouts.master')

@section('title')
<h6 class="slim-pagetitle"> {{ trans('dashboard.Welcome') .', '.auth()->user()->name }}</h6>
@endsection

@section('content')
<div class="section-wrapper">
    <div class="row row-xs mg-t-10">
        <div class="col-lg-12 col-xl-12">
            <div class="row row-xs">
                @if($pendingPosts)
                <div class="col-md-12 mg-t-10">
                    <div class="card pd-20">
                        <div class="card-header">
                            <h6 class="slim-card-title">{{ trans('dashboard.Pending posts')}}</h6>
                        </div><!-- card-header -->
                        <div class="table-responsive">
                            <table class="table mg-b-0 tx-13">
                                <thead>
                                    <tr>
                                        <th class="wd-5p">{{trans('posts.ID')}} </th>
                                        <th class="wd-30p">{{trans('posts.Title')}} </th>
                                        <th class="wd-10p">{{trans('posts.Views count')}} </th>
                                        <th class="wd-5p">{{trans('posts.Active')}} </th>
                                        <th class="wd-10p">{{trans('posts.Created by')}} </th>
                                        <th class="wd-15p">{{trans('posts.Created at')}}</th>
                                        <th class="wd-20p">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pendingPosts as $row)
                                    <tr>
                                        <td class="center">{{$row->id}}</td>
                                        <td class="center">{{str_limit($row->title,50)}}</td>
                                        <td class="center">{{$row->views}}</td>
                                        <td class="center"><img src="backend/img/{{($row->is_active)?'check.png':'close.png'}}"></td>
                                        <td class="center">{{@$row->creator->name}}</td>
                                        <td class="center">{{$row->created_at}}</td>
                                        <td class="center">

                                            @if(can('edit-posts'))
                                            <a class="btn btn-success btn-xs" href="admin/posts/edit/{{$row->id}}" title="{{trans('posts.Edit')}}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            @endif

                                            @if(can('view-posts'))
                                            <a class="btn btn-primary btn-xs" href="admin/posts/view/{{$row->id}}" title="{{trans('posts.View')}}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            @endif

                                            @if(can('delete-posts'))
                                            <a class="btn btn-danger btn-xs" href="admin/posts/delete/{{$row->id}}" title="{{trans('posts.Delete')}}" data-confirm="{{trans('posts.Are you sure you want to delete this item')}}?">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- table-responsive -->
                    </div><!-- card -->
                </div><!-- col-5 -->
                @endif


                @if($top_search_sections)
                <div class="col-md-6 mg-t-10">
                    <div class="card pd-20">
                        <div class="card-header">
                            <h6 class="slim-card-title">{{ trans('dashboard.Top search sections')}}</h6>
                        </div><!-- card-header -->
                        <div class="table-responsive">
                            <table class="table mg-b-0 tx-13">
                                <thead>
                                    <tr class="tx-10">
                                        <th class="wd-25p">{{trans('dashboard.Section')}}</th>
                                        <th class="wd-15p">{{trans('dashboard.Total Search')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($top_search_sections as $row)
                                    <tr>
                                        <td class="center">{{@$sections[$row->value]}}</td>
                                        <td class="valign-middle">{{$row->total}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- table-responsive -->
                    </div><!-- card -->
                </div><!-- col-5 -->
                @endif


                @if($top_search_countries)
                <div class="col-md-6 mg-t-10">
                    <div class="card pd-20">
                        <div class="card-header">
                            <h6 class="slim-card-title">{{ trans('dashboard.Top search countries')}}</h6>
                        </div><!-- card-header -->
                        <div class="table-responsive">
                            <table class="table mg-b-0 tx-13">
                                <thead>
                                    <tr class="tx-10">
                                        <th class="wd-25p">{{trans('dashboard.Country')}}</th>
                                        <th class="wd-15p">{{trans('dashboard.Total Search')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($top_search_countries as $row)
                                    <tr>
                                        <td class="center">{{@$countries[$row->value]}}</td>
                                        <td class="valign-middle">{{$row->total}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- table-responsive -->
                    </div><!-- card -->
                </div><!-- col-5 -->
                @endif


                @if($top_search_times)
                <div class="col-md-6 mg-t-10">
                    <div class="card pd-20">
                        <div class="card-header">
                            <h6 class="slim-card-title">{{ trans('dashboard.Top search times')}}</h6>
                        </div><!-- card-header -->
                        <div class="table-responsive">
                            <table class="table mg-b-0 tx-13">
                                <thead>
                                    <tr class="tx-10">
                                        <th class="wd-25p">{{trans('dashboard.Time')}}</th>
                                        <th class="wd-15p">{{trans('dashboard.Total Search')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($top_search_times as $row)
                                    <tr>
                                        <td class="center">{{@$times[$row->value]}}</td>
                                        <td class="valign-middle">{{$row->total}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- table-responsive -->
                    </div><!-- card -->
                </div><!-- col-5 -->
                @endif

                @if($top_search_where)
                <div class="col-md-6 mg-t-10">
                    <div class="card pd-20">
                        <div class="card-header">
                            <h6 class="slim-card-title">{{ trans('dashboard.Top search where')}}</h6>
                        </div><!-- card-header -->
                        <div class="table-responsive">
                            <table class="table mg-b-0 tx-13">
                                <thead>
                                    <tr class="tx-10">
                                        <th class="wd-25p">{{trans('dashboard.Where')}}</th>
                                        <th class="wd-15p">{{trans('dashboard.Total Search')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($top_search_where as $row)
                                    <tr>
                                        <td class="center">{{@$where[$row->value]}}</td>
                                        <td class="valign-middle">{{$row->total}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- table-responsive -->
                    </div><!-- card -->
                </div><!-- col-5 -->
                @endif


                @if($top_search_budget)
                <div class="col-md-6 mg-t-10">
                    <div class="card pd-20">
                        <div class="card-header">
                            <h6 class="slim-card-title">{{ trans('dashboard.Top search budget')}}</h6>
                        </div><!-- card-header -->
                        <div class="table-responsive">
                            <table class="table mg-b-0 tx-13">
                                <thead>
                                    <tr class="tx-10">
                                        <th class="wd-25p">{{trans('dashboard.Budget')}}</th>
                                        <th class="wd-15p">{{trans('dashboard.Total Search')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($top_search_budget as $row)
                                    <tr>
                                        <td class="center">{{$row->value}}</td>
                                        <td class="valign-middle">{{$row->total}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- table-responsive -->
                    </div><!-- card -->
                </div><!-- col-5 -->
                @endif

            </div><!-- row -->
        </div><!-- col-9 -->
    </div><!-- row -->
</div>
@endsection
