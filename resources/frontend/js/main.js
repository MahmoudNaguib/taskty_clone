(function ($) {
    "use strict";

    /*-----------------------------
     Menu Stick
     ---------------------------------*/
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 1) {
            $('.sticker').addClass("stick");
        } else {
            $('.sticker').removeClass("stick");
        }
    });

    /*----------------------------
     Toogle Search
     ------------------------------ */
    // Handle click on toggle search button
    $('.header-search').on('click', function () {
        $('.search').toggleClass('open');
        return false;
    });

    /*----------------------------
     jQuery MeanMenu
     ------------------------------ */
    jQuery('nav#dropdown').meanmenu();


    /*----------------------------
     Nivo Slider Active
     ------------------------------ */
    $('#nivoslider').nivoSlider({
        effect: 'random',
        slices: 15,
        boxCols: 10,
        boxRows: 10,
        animSpeed: 500,
        pauseTime: 5000,
        startSlide: 0,
        directionNav: true,
        controlNavThumbs: false,
        pauseOnHover: false,
        manualAdvance: true
    });
    /*----------------------------
     Wow js active
     ------------------------------ */
    new WOW().init();

    /*--------------------------
     ScrollUp
     ---------------------------- */
    $.scrollUp({
        scrollText: '<i class="fa fa-angle-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });

    /*--------------------------
     Counter Up
     ---------------------------- */
    $('.counter').counterUp({
        delay: 70,
        time: 5000
    });

    /*--------------------------------
     Testimonial Slick Carousel
     -----------------------------------*/
    $('.testimonial-text-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        draggable: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    /*------------------------------------
     Testimonial Slick Carousel as Nav
     --------------------------------------*/
    $('.testimonial-image-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.testimonial-text-slider',
        dots: false,
        arrows: true,
        centerMode: true,
        focusOnSelect: true,
        centerPadding: '10px',
        responsive: [
            {
                breakpoint: 450,
                settings: {
                    dots: false,
                    slidesToShow: 3,
                    centerPadding: '0px',
                }
            },
            {
                breakpoint: 420,
                settings: {
                    autoplay: true,
                    dots: false,
                    slidesToShow: 1,
                    centerMode: false,
                }
            }
        ]
    });

    /*------------------------------------
     Textilate Activation
     --------------------------------------*/
    $('.tlt').textillate({
        loop: true,
        minDisplayTime: 2500
    });

    /*------------------------------------
     Video Player
     --------------------------------------*/
    $(".player").YTPlayer({
        showControls: false
    });

    $(".player-small").YTPlayer({
        showControls: false
    });

    /*------------------------------------
     MailChimp
     --------------------------------------*/
    $('#mc-form').ajaxChimp({
        language: 'en',
        callback: mailChimpResponse,
        // ADD YOUR MAILCHIMP URL BELOW HERE!
        url: 'http://themeshaven.us8.list-manage.com/subscribe/post?u=759ce8a8f4f1037e021ba2922&amp;id=a2452237f8'
    });

    function mailChimpResponse(resp) {

        if (resp.result === 'success') {
            $('.mailchimp-success').html('' + resp.msg).fadeIn(900);
            $('.mailchimp-error').fadeOut(400);

        } else if (resp.result === 'error') {
            $('.mailchimp-error').html('' + resp.msg).fadeIn(900);
        }
    }

    /*------------------------------------
     ColorSwitcher
     --------------------------------------*/
    $('.ec-handle').on('click', function () {
        $('.ec-colorswitcher').trigger('click')
        $(this).toggleClass('btnclose');
        $('.ec-colorswitcher').toggleClass('sidebarmain');
        return false;
    });
    $('.ec-boxed,.pattren-wrap a,.background-wrap a').on('click', function () {
        $('.as-mainwrapper').addClass('wrapper-boxed');
        $('.as-mainwrapper').removeClass('wrapper-wide');
    });
    $('.ec-wide').on('click', function () {
        $('.as-mainwrapper').addClass('wrapper-wide');
        $('.as-mainwrapper').removeClass('wrapper-boxed');
    });

    /*------------------------------------
     Magnific Popup Active
     --------------------------------------*/
    $('.popup-image').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
        // other options
    });



    //////////////////////////////////////////////////////////////// Plugins
    function confirmation() {
        $('a[data-confirm]').on('click', function () {
            var href = $(this).attr('href');
            var title = $(this).data("title");
            if (!$('#dataConfirmModal').length) {
                $('body').append('  <div class="modal fade" id="dataConfirmModal" tabindex="-1" role="document">\n\
						   <div class="modal-dialog modal-sm" role="document">\n\
						    <div class="modal-content bd-0">\n\
						      <div class="modal-body tx-center pd-y-20 pd-x-20">\n\
						        Hi there, I am a Modal Example for Dashio Admin Panel.\n\
						      </div>\n\
						      <div class="modal-footer justify-content-center">\n\
						        <button type="button" class="btn btn-master" data-dismiss="modal">Cancel</button>\n\
						        <a class="btn btn-danger" id="dataConfirmOK">Yes</a>\n\
						      </div>\n\
						    </div>\n\
						  </div>\n\
						</div> ');
            }
            $('#dataConfirmModal').find('.modal-body').html('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>' + $(this).attr('data-confirm'));
            $('#dataConfirmOK').attr('href', href);
            $('#dataConfirmModal').modal({show: true});
            return false;
        });
    }
    confirmation();
    // Datatable
    $('.select2').select2();
    var table = $('.dataTable').DataTable({
        responsive: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
        },
        "order": [[0, 'desc']]
    });
    table.on('draw', function () {
        confirmation();
    });
    $('.dataTables_length select').select2({minimumResultsForSearch: Infinity});
    // Select2

    $('.datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: 'c-90:c+10'
    });
    ////////// Tags input
    $('.tags').tagsinput();
    ///////// sortable
    $('.editor').trumbowyg({
        svgPath: 'backend/svg/icons.svg',
        btns: [
            // ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            //  ['superscript', 'subscript'],
            ['link'],
            //  ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            // ['horizontalRule'],
            ['removeformat'],
                    //['fullscreen']
        ]
    });

})(jQuery); 