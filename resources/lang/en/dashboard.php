<?php 

return [
    '0' => '1',
    'Welcome' => 'Welcome',
    'My dashboard' => 'My dashboard',
    'Top search sections' => 'Top search sections',
    'Section' => 'Section',
    'Total Search' => 'Total Search',
    'Top search countries' => 'Top search countries',
    'Country' => 'Country',
    'Top search times' => 'Top search times',
    'Time' => 'Time',
    'Top search budget' => 'Top search budget',
    'Budget' => 'Budget',
    'Top search where' => 'Top search where',
    'Where' => 'Where',
    'Pending posts' => 'Pending posts',
];