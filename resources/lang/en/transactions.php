<?php 

return [
    '0' => '1',
    'ID' => 'ID',
    'Task' => 'Task',
    'Amount' => 'Amount',
    'Tasker' => 'Tasker',
    'Creator' => 'Creator',
    'Status' => 'Status',
    'Created at' => 'Created at',
    'View' => 'View',
    'Delete' => 'Delete',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'There is no results' => 'There is no results',
    'Created by' => 'Created by',
];