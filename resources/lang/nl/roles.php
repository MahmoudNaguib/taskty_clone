<?php 

return [
    '0' => '1',
    'Title' => 'Title',
    'Choose at least 1 permission' => 'Choose at least 1 permission',
    'ID' => 'ID',
    'Permissions' => 'Permissions',
    'Created at' => 'Created at',
    'View' => 'View',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'Created by' => 'Created by',
];