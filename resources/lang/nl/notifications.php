<?php 

return [
    '0' => '1',
    'Are you sure you want to delete all notifications' => 'Are you sure you want to delete all notifications',
    'ID' => 'ID',
    'Message' => 'Message',
    'URL' => 'URL',
    'Created at' => 'Created at',
    'View' => 'View',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'There is no results' => 'There is no results',
    'Url' => 'Url',
];