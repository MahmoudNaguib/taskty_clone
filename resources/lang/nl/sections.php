<?php 

return [
    '0' => '1',
    'Parent' => 'Parent',
    'Title' => 'Title',
    'Image' => 'Image',
    'Is active' => 'Is active',
    'ID' => 'ID',
    'Parent section' => 'Parent section',
    'Active' => 'Active',
    'Created at' => 'Created at',
    'Edit' => 'Edit',
    'View' => 'View',
    'Delete' => 'Delete',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'There is no results' => 'There is no results',
    'Created by' => 'Created by',
];