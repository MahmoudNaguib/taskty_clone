<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => 'The field must be accepted.',
    'after' => 'The field must be a date after :date.',
    'after_or_equal' => 'The field must be a date after or equal to :date.',
    'boolean' => 'The field must be true or false.',
    'confirmed' => 'The field confirmation does not match.',
    'date' => 'The field is not a valid date.',
    'email' => 'The field must be a valid email address.',
    'mobile' => 'The field must be a valid mobile number',
    'exists' => 'The selected field is invalid.',
    'file' => 'The field must be a file.',
    'image' => 'The field must be an image.',
    'integer' => 'The field must be an integer.',
    'mimes' => 'The field must be a file of type: :values.',
    'mimetypes' => 'The field must be a file of type: :values.',
    'min' => [
        'numeric' => 'The field must be at least :min.',
        'file' => 'The field must be at least :min kilobytes.',
        'string' => 'The field must be at least :min characters.',
        'array' => 'The field must have at least :min items.',
    ],
    'not_in' => 'The selected field is invalid.',
    'required' => 'The field is required.',
    'same' => 'The field and :other must match.',
    'size' => [
        'numeric' => 'The field must be :size.',
        'file' => 'The field must be :size kilobytes.',
        'string' => 'The field must be :size characters.',
        'array' => 'The field must contain :size items.',
    ],
    'string' => 'The field must be a string.',
    'unique' => 'The field has already been taken.',
    'url' => 'The field format is invalid.',
];
