<?php

return [
    'countries' => ['create', 'edit', 'view', 'delete'],
    'contact_messages' => ['create', 'edit', 'view', 'delete'],
    'subscribers' => ['create', 'edit', 'view', 'delete'],
    'users' => ['edit', 'view', 'delete', 'create'],
    'posts' => ['create', 'edit', 'view', 'delete'],
    'comments' => ['view', 'delete'],
    'sections' => ['create', 'edit', 'view', 'delete'],
    'tasks' => ['create', 'edit', 'view', 'delete'],
    'offers' => ['view', 'delete'],
    'transactions' => ['create', 'view', 'delete'],
    'questions' => ['view', 'delete'],
];
