<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentsControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_comments() {
        dump('test_list_comments');
        $user = \App\Models\User::where('type','admin')->first();
        $latest = \App\Models\Comment::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/comments')
                ->assertStatus(200)
                ->assertSee('comments');
    }


    public function test_delete_comments() {
        dump('test_delete_comments');
        $user = \App\Models\User::where('type','admin')->first();
        $record = \App\Models\Comment::create(factory(\App\Models\Comment::class)->make()->toArray());
        $row = factory(\App\Models\Comment::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/comments/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

    public function test_view_comments() {
        dump('view_delete_comments');
        $user = \App\Models\User::where('type','admin')->first();
        $record = \App\Models\Comment::create(factory(\App\Models\Comment::class)->make()->toArray());
        $row = factory(\App\Models\Comment::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/comments/view/' . $record->id)
                ->assertStatus(200);
        $record->forceDelete();
    }

}
