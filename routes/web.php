<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/* Route::get('/', function () {
  return view('welcome');
  });
 */
Route::group(['prefix' => (app()->environment() == 'testing') ? 'en' : LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']], function() {
    AdvancedRoute::controller('auth', 'AuthController');
    Route::group(['middleware' => ['auth']], function() {
        AdvancedRoute::controller('profile', 'ProfileController');
        AdvancedRoute::controller('notifications', 'NotificationsController');
        Route::group(['prefix' => 'admin'], function() {
            Route::group(['middleware' => ['isAdmin']], function() {
                AdvancedRoute::controller('roles', 'Admin\RolesController');
                AdvancedRoute::controller('users', 'Admin\UsersController');
                AdvancedRoute::controller('posts', 'Admin\PostsController');
                AdvancedRoute::controller('comments', 'Admin\CommentsController');
                AdvancedRoute::controller('faqs', 'Admin\FaqsController');
                AdvancedRoute::controller('contact_messages', 'Admin\ContactMessagesController');
                AdvancedRoute::controller('subscribers', 'Admin\SubscribersController');
                AdvancedRoute::controller('countries', 'Admin\CountriesController');
                AdvancedRoute::controller('tasks', 'Admin\TasksController');
                AdvancedRoute::controller('offers', 'Admin\OffersController');
                AdvancedRoute::controller('questions', 'Admin\QuestionsController');
                AdvancedRoute::controller('rates', 'Admin\RatesController');
                AdvancedRoute::controller('configs', 'Admin\ConfigsController');
                AdvancedRoute::controller('search', 'Admin\SearchController');
                AdvancedRoute::controller('translator', 'Admin\TranslatorController');
                AdvancedRoute::controller('sections', 'Admin\SectionsController');
                AdvancedRoute::controller('transactions', 'Admin\TransactionsController');
                AdvancedRoute::controller('ajax', 'Admin\AjaxController');
                AdvancedRoute::controller('/', 'Admin\DashBoardController');
            });
        });
        AdvancedRoute::controller('comments', 'CommentsController');
        AdvancedRoute::controller('ajax', 'AjaxController');
        AdvancedRoute::controller('/offers', 'OffersController');
        AdvancedRoute::controller('/questions', 'QuestionsController');
    });
    AdvancedRoute::controller('/tasks', 'TasksController');
    AdvancedRoute::controller('/blog', 'BlogController');
    AdvancedRoute::controller('/favourites', 'FavouritesController');
    AdvancedRoute::controller('/bio', 'BioController');
    AdvancedRoute::controller('/posts', 'PostsController');
    AdvancedRoute::controller('/contact_messages', 'ContactMessagesController');
    AdvancedRoute::controller('/subscribers', 'SubscribersController');
    AdvancedRoute::controller('/transactions', 'TransactionsController');
    Route::get('/about', 'PagesController@about');
    Route::get('/terms', 'PagesController@terms');
    Route::get('/privacy', 'PagesController@privacy');
    Route::get('/contact', 'PagesController@contact');
    AdvancedRoute::controller('/', 'HomeController');
});


Route::prefix('api')->group(function () {
    require_once __DIR__ . '/api.php';
});
